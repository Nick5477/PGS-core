﻿using System.Collections.Generic;

namespace Domain.Services.Course
{
    public interface ICourseService
    {
        void BindUserToCourse(Domain.Entities.User user, Domain.Entities.Course course);

        IEnumerable<Domain.Entities.Course> GetAllCourses();

        bool IsBindedUserToCourseOnMoodle(Domain.Entities.User user, Domain.Entities.Course course);

        bool IsBindedUserToLocalCourse(Entities.User user, Domain.Entities.Course course);

        IEnumerable<Entities.Course> GetCoursesFromMoodle();

        Entities.Course GetCourseByMoodleId(int moodleId);

        IEnumerable<Entities.Course> GetCoursesByUsername(string username);
    }
}
