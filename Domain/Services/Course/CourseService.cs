﻿using System;
using System.Collections.Generic;
using Domain.Commands;
using Domain.Commands.Contexts.Course;
using Domain.Queries;
using Domain.Queries.Criterion.Course;
using Domain.Queries.Criterion.User;
using Domain.Repositories;

namespace Domain.Services.Course
{
    public class CourseService : IEntityService<Entities.Course>, ICourseService
    {
        private readonly ICommandBuilder _commandBuilder;
        private readonly IQueryBuilder _queryBuilder;
        private readonly IRepository<Entities.Course> _repository;

        public CourseService(
            IRepository<Entities.Course> repository,
            ICommandBuilder commandBuilder,
            IQueryBuilder queryBuilder)
        {
            _commandBuilder = commandBuilder;
            _queryBuilder = queryBuilder;
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }
        public void Add(Entities.Course entity)
        {
            _repository.Add(entity);
        }

        public Entities.Course Get(string id)
        {
            return _repository.Get(id);
        }

        public void Delete(string id)
        {
            throw new System.NotImplementedException();
        }

        public void BindUserToCourse(Domain.Entities.User user, Domain.Entities.Course course)
        {
            _commandBuilder.Execute(new BindUserToCourseCommandContext()
            {
                Course = course,
                User = user
            });
        }

        public IEnumerable<Entities.Course> GetAllCourses()
        {
            return _queryBuilder
                .For<IEnumerable<Entities.Course>>()
                .With(new GetAllCoursesCriterion());
        }

        public bool IsBindedUserToCourseOnMoodle(Entities.User user, Domain.Entities.Course course)
        {
            return _queryBuilder
                .For<bool>()
                .With(new CheckUserBindedToMoodleCourseCriterion()
                {
                    Username = user.Nickname,
                    MoodleId = course.MoodleId
                });
        }

        public bool IsBindedUserToLocalCourse(Entities.User user, Entities.Course course)
        {
            return _queryBuilder
                .For<bool>()
                .With(new IsBindedUserToCourseCriterion()
                {
                    CourseId = course.Id,
                    UserId = user.Id
                });
        }

        public IEnumerable<Entities.Course> GetCoursesFromMoodle()
        {
            return _queryBuilder.For<IEnumerable<Entities.Course>>()
                .With(new SyncCoursesCriterion());
        }

        public Entities.Course GetCourseByMoodleId(int moodleId)
        {
            return _queryBuilder.For<Entities.Course>()
                .With(new GetCourseByMoodleIdCriterion()
                {
                    MoodleId = moodleId
                });
        }

        public IEnumerable<Entities.Course> GetCoursesByUsername(string username)
        {
            return _queryBuilder
                .For<IEnumerable<Entities.Course>>()
                .With(new GetCoursesByUserCriterion()
                {
                    Username = username
                });
        }
    }
}
