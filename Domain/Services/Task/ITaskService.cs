﻿namespace Domain.Services.Task
{
    using System.Collections.Generic;

    public interface ITaskService
    {
        Entities.Task GetTaskByMoodleId(long moodleId);

        IEnumerable<Entities.Task> GetTasksFromMoodle(long moodleId);

        IEnumerable<Entities.Task> GetTasksByCourseId(string courseId);

        void Update(Entities.Task entity);

        void CalculateOrdinalGrades(int MaxWorks, int qtStudents);
    }
}
