﻿namespace Domain.Services.Task
{
    using System.Collections.Generic;
    using Queries;
    using Queries.Criterion.Task;
    using Repositories;
    using System.Linq;

    public class TaskService : IEntityService<Entities.Task>, ITaskService
    {
        private readonly IRepository<Entities.Task> _repository;
        private readonly IQueryBuilder _queryBuilder;

        public TaskService(
            IRepository<Entities.Task> repository,
            IQueryBuilder queryBuilder)
        {
            _repository = repository;
            _queryBuilder = queryBuilder;
        }
        public void Add(Entities.Task entity)
        {
            _repository.Add(entity);
        }

        public Entities.Task Get(string id)
        {
            return _repository.Get(id);
        }

        public void Delete(string id)
        {
            throw new System.NotImplementedException();
        }


        public Entities.Task GetTaskByMoodleId(long moodleId)
        {
            return _queryBuilder
                .For<Entities.Task>()
                .With(new GetTaskByMoodleIdCriterion()
                {
                    TaskMoodleId = moodleId
                });
        }

        public IEnumerable<Entities.Task> GetTasksFromMoodle(long moodleId)
        {
            return _queryBuilder
                .For<IEnumerable<Entities.Task>>()
                .With(new SyncTasksCriterion()
                {
                    CourseMoodleId = moodleId
                });
        }

        public IEnumerable<Entities.Task> GetTasksByCourseId(string courseId)
        {
            return _queryBuilder
                .For<IEnumerable<Entities.Task>>()
                .With(new GetTaskByCourseIdCriterion()
                {
                    CourseId = courseId
                });
        }

        public void Update(Entities.Task entity)
        {
            _repository.Update(entity);
        }

        //характеристическая функция (функция принадлежности)
        private double CalculateQuentifier(double x, double a, double b)
        {
            if (x < a)
                return 0;

            if (x >= a && x <= b)
                return (x - a) / (b - a);

            return 1;
        }

        private List<double> CalculateWeights(int k, int n, double a, double b)
        {
            List<double> result = new List<double>();
            for (int i = 1; i <= k; i++)
            {
                result.Add(CalculateQuentifier((double)i / (double)n, a, b) - CalculateQuentifier((double)(i - 1) / (double)n, a, b));
            }

            return result;
        }

        private double GetOWA(List<double> weights, List<double?> preferences)
        {
            double result = 0;
            preferences.Sort();
            for (int i = 0; i < preferences.Count; i++)
            {
                result += weights.ElementAt(i) * preferences.ElementAt(i).Value;
            }

            return result;
        }

        private double GetPhiNetFlow(double[,] preferences, int currentWork)
        {
            double sum1 = 0, sum2 = 0;
            for (int i = 0; i < preferences.GetLength(0); i++)
            {
                if (i != currentWork)
                {
                    sum1 += preferences[currentWork, i];
                    sum2 += preferences[i, currentWork];
                }
            }

            return sum1 - sum2;
        }

        public void CalculateOrdinalGrades(int MaxWorks, int qtStudents)
        {
            List<Entities.OrdinalGrade> grades = new List<Entities.OrdinalGrade>();
            //каждому студенту присваивается номер
            Dictionary<Entities.User, int> mapping = new Dictionary<Entities.User, int>();
            int countStudents = 0;
            foreach (var grade in grades)
            {
                if (!mapping.ContainsKey(grade.FirstStudent))
                {
                    mapping.Add(grade.FirstStudent, countStudents);
                    countStudents++;
                }
            }
            double a = 0.2, b = 0.8;
            //вычисляются веса (вес - это число от 0 до 1, которое характеризует вес i-ой оценки в порядке убывания)
            var weights = CalculateWeights(MaxWorks, qtStudents, a, b);

            double[,] preferences = new double[countStudents, countStudents];
            for (int i = 0; i < countStudents; i++)
            {
                for (int j = 0; j < countStudents; j++)
                {
                    if (i != j)
                    {
                        //вычисляется массив оценок (коэф-тов предпочтения) * массив весов для каждого студента
                        List<double?> individualFPR =
                            grades
                                .Where(
                                    grade => grade.FirstStudent.Id == mapping.First(x => x.Value == i).Key.Id
                                             && grade.SecondStudent.Id == mapping.First(x => x.Value == j).Key.Id)
                                .Select(grade => grade.Grade).ToList();
                        preferences[i, j] = GetOWA(weights, individualFPR);
                    }
                    else
                    {
                        preferences[i, j] = 0.5;
                    }

                }
            }

            List<double> resultGrades = new List<double>();

            for (int i = 0; i < qtStudents; i++)
            {
                resultGrades.Add(GetPhiNetFlow(preferences, i));
            }

            //вычисление абсолютных оценок
        }
    }
}
