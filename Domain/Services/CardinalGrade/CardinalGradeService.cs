﻿namespace Domain.Services.CardinalGrade
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Domain.Commands;
    using Domain.Commands.Contexts.CardinalGrade;
    using Domain.Entities;
    using Domain.Queries;
    using Domain.Queries.Criterion.CardinalGrade;
    using Domain.Repositories;
    using Domain.Services.Submission;
    using Domain.Services.User;

    public class CardinalGradeService : IEntityService<CardinalGrade>, ICardinalGradeService
    {
        private readonly IQueryBuilder _queryBuilder;

        private readonly ICommandBuilder _commandBuilder;

        private readonly IRepository<CardinalGrade> _repository;

        private readonly IUserService _userService;

        private readonly ISubmissionService _submissionService;

        public CardinalGradeService(
            IQueryBuilder queryBuilder,
            ICommandBuilder commandBuilder,
            IRepository<CardinalGrade> repository,
            IUserService userService,
            ISubmissionService submissionService)
        {
            _queryBuilder = queryBuilder;
            _commandBuilder = commandBuilder;
            _repository = repository;
            _userService = userService;
            _submissionService = submissionService;
        }

        public void Add(CardinalGrade entity)
        {
            _repository.Add(entity);
        }

        public CardinalGrade Get(string id)
        {
            return _repository.Get(id);
        }

        public void Delete(string id)
        {
            _repository.Delete(_repository.Get(id));
        }

        public IEnumerable<CardinalGrade> GetCardinalGradesByTask(Task task)
        {
            return _queryBuilder
                .For<IEnumerable<CardinalGrade>>()
                .With(new GetCardinalGradesByTaskCriterion
                {
                    Task = task
                });
        }

        public void CalculateGrades(Task task, double alpha, double beta)
        {
            IEnumerable<CardinalGrade> cardinalGrades = GetCardinalGradesByTask(task);
            var users = _userService.GetUsersByCourse(task.Course.Id).Where(u => u.Role.Name != "teacher").ToList();
            var userIds = users.Select(u => u.Id).ToArray();
            int n = users.Count;
            double[,] gradeMatrix = new double[n, n];
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    var grade =
                        cardinalGrades
                            .FirstOrDefault(
                                g =>
                                    g.Grader.Id == userIds[j]
                                    && g.Submission.User.Id == userIds[i]);
                    gradeMatrix[i, j] = grade == null ? -1 : (double)grade.Grade / (double)task.MaxGrade;
                }
            }
            double[] resultGrades = new double[n];
            int countGrades = 0;
            
            //first iteration PeerRank
            for (int i = 0; i < n; i++)
            {
                countGrades = 0;
                double sum = 0;
                for (int j = 0; j < n; j++)
                {
                    if (gradeMatrix[i, j] >= -0.9)
                    {
                        sum += gradeMatrix[i, j];
                        countGrades++;
                    }
                }
                if (countGrades > 0)
                {
                    resultGrades[i] = 1.0 / (double)countGrades * sum;
                }
            }

            
            //next iterations PeerRank
            for (int k = 0; k < 30; k++)
            {
                for (int i = 0; i < resultGrades.Length; i++)
                {
                    countGrades = 0;
                    double firstEquationTerm = (1 - alpha - beta) * resultGrades[i];
                    double sum = 0;
                    double resultGradesSum = 0;
                    for (int j = 0; j < resultGrades.Length; j++)
                    {
                        if (gradeMatrix[i, j] >= -0.9)
                        {
                            sum += resultGrades[j] * gradeMatrix[i, j];
                            resultGradesSum += resultGrades[j];
                            countGrades++;
                        }
                    }

                    double secondEquationTerm = resultGradesSum == 0 || sum == 0 ? 0 : alpha / (double)resultGradesSum * sum;
                    sum = 0;
                    for (int j = 0; j < resultGrades.Length; j++)
                    {
                        if (gradeMatrix[j, i] >= -0.9)
                        {
                            sum += 1 - Math.Abs(gradeMatrix[j, i] - resultGrades[j]);
                        }
                    }
                    double thirdEquationTerm = countGrades == 0 || sum == 0 ? 0 : beta / (double)countGrades * sum;
                    resultGrades[i] = firstEquationTerm + secondEquationTerm + thirdEquationTerm;
                }
            }

            CreateCardinalGrades(resultGrades, userIds, task);
        }

        private void CreateCardinalGrades(double[] grades, string[] userIds, Task task)
        {
            var submissions = _submissionService.GetSubmissionsByTask(task);

            for (int i = 0; i < userIds.Length; i++)
            {
                var submission = submissions.FirstOrDefault(s => s.User.Id == userIds[i]);
                if (submission != null)
                {
                    submission.SummaryGrade = Convert.ToInt32(Math.Round(grades[i] * task.MoodleMaxGrade));
                }
            }

            foreach (var submission in submissions)
            {
                _submissionService.Update(submission);
            }
        }

        public IEnumerable<CardinalGrade> GetCardinalGradesByTaskAndUser(Task task, User user)
        {
            return _queryBuilder
                .For<IEnumerable<CardinalGrade>>()
                .With(
                    new GetCardinalGradesByTaskAndUserCriterion
                    {
                        Task = task,
                        User = user
                    });
        }

        public CardinalGrade GetCardinalGradeBySubmissionAndUser(Submission submission, User user)
        {
            return
                _queryBuilder
                    .For<CardinalGrade>()
                    .With(
                        new GetCardinalGradeBySubmissionAndUserCriterion
                        {
                            Submission = submission,
                            User = user
                        });
        }

        public void DeleteCardinalAnswers(CardinalGrade grade)
        {
            _commandBuilder
                .Execute(
                new DeleteCardinalAnswersCommandContext
                {
                    CardinalGrade = grade
                });
        }
    }
}
