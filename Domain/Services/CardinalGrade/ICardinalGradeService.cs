﻿namespace Domain.Services.CardinalGrade
{
    using System.Collections.Generic;

    using Domain.Entities;

    public interface ICardinalGradeService
    {
        IEnumerable<Entities.CardinalGrade> GetCardinalGradesByTask(Entities.Task task);

        void CalculateGrades(Entities.Task task, double alpha, double beta);

        IEnumerable<Entities.CardinalGrade> GetCardinalGradesByTaskAndUser(Entities.Task task, Entities.User user);

        Entities.CardinalGrade GetCardinalGradeBySubmissionAndUser(Entities.Submission submission, Entities.User user);

        void DeleteCardinalAnswers(CardinalGrade grade);
    }
}