﻿using Domain.Repositories;

namespace Domain.Services.File
{
    public class FileService : IEntityService<Entities.File>
    {
        private readonly IRepository<Entities.File> _repository;

        public FileService(IRepository<Entities.File> repository)
        {
            _repository = repository;
        }
        public void Add(Entities.File entity)
        {
            _repository.Add(entity);
        }

        public Entities.File Get(string id)
        {
            return _repository.Get(id);
        }

        public void Delete(string id)
        {
            throw new System.NotImplementedException();
        }
    }
}
