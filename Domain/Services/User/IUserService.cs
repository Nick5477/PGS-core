﻿namespace Domain.Services.User
{
    using System.Collections.Generic;

    using Domain.Entities;

    public interface IUserService
    {
        User GetUserByName(string username);

        IEnumerable<User> GetAllLocalUsers();

        IEnumerable<User> GetUsersByCourse(string courseId);

        User GetUserBySubmission(Submission submission);
    }
}
