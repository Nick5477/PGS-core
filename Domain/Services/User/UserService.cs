﻿namespace Domain.Services.User
{
    using System;
    using System.Collections.Generic;

    using Domain.Entities;
    using Domain.Queries;
    using Domain.Queries.Criterion.User;
    using Domain.Repositories;

    public class UserService : IEntityService<User>, IUserService
    {
        private readonly IQueryBuilder _queryBuilder;
        private readonly IRepository<User> _repository;

        public UserService(
            IRepository<User> repository,
            IQueryBuilder queryBuilder)
        {
            _queryBuilder = queryBuilder;
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public void Add(User entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            _repository.Add(entity);
        }

        public void Delete(string id)
        {
            throw new NotImplementedException();
        }

        public User Get(string id)
        {
            return _repository.Get(id);
        }

        public User GetUserByName(string username)
        {
            return _queryBuilder
                .For<User>()
                .With(new GetUserByNameCriterion
                {
                    Username = username
                });
        }

        public IEnumerable<User> GetAllLocalUsers()
        {
            return _queryBuilder
                .For<IEnumerable<User>>()
                .With(new GetAllUsersCriterion());
        }

        public IEnumerable<User> GetUsersByCourse(string courseId)
        {
            return _queryBuilder
                .For<IEnumerable<User>>()
                .With(new GetUsersByCourseCriterion
                {
                    CourseId = courseId
                });
        }

        public User GetUserBySubmission(Submission submission)
        {
            return
                _queryBuilder
                    .For<User>()
                    .With(
                        new GetUserBySubmissionCriterion()
                        {
                            Submission = submission
                        });
        }
    }
}
