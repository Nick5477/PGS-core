﻿namespace Domain.Services.OrdinalGrade
{
    using System.Collections.Generic;
    using Queries;
    using Queries.Criterion.OrdinalGrade;
    using Repositories;
    using System.Linq;
    using Commands;
    using UnitOfWork;
    using Submission;
    using System;

    public class OrdinalGradeService : IEntityService<Entities.OrdinalGrade>, IOrdinalGradeService
    {
        private readonly IRepository<Entities.OrdinalGrade> _repository;
        private readonly IQueryBuilder _queryBuilder;
        private readonly ICommandBuilder _commandBuilder;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IEntityService<Entities.User> _userEntityService;
        private readonly ISubmissionService _submissionService;

        public OrdinalGradeService(
            IRepository<Entities.OrdinalGrade> repository,
            IQueryBuilder queryBuilder,
            ICommandBuilder commandBuilder,
            IUnitOfWorkFactory unitOfWorkFactory,
            IEntityService<Entities.User> userEntityService,
            ISubmissionService submissionService)
        {
            _repository = repository;
            _queryBuilder = queryBuilder;
            _commandBuilder = commandBuilder;
            _unitOfWorkFactory = unitOfWorkFactory;
            _userEntityService = userEntityService;
            _submissionService = submissionService;
        }

        public void Add(Entities.OrdinalGrade entity)
        {
            _repository.Add(entity);
        }

        public Entities.OrdinalGrade Get(string id)
        {
            throw new System.NotImplementedException();
        }

        public void Delete(string id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Entities.OrdinalGrade> GetOrdinalGradesByUsernameAndTask(string username, Entities.Task task)
        {
            return _queryBuilder
                .For<IEnumerable<Entities.OrdinalGrade>>()
                .With(
                    new GetOrdinalGradesByUsernameAndTaskCriterion()
                    {
                        Task = task,
                        Username = username
                    });
        }

        public bool IsGradedSubmissionsByUser(string studentId, string taskId)
        {
            return _queryBuilder
                .For<bool>()
                .With(
                    new IsGradedSubmissionsByUserCriterion()
                    {
                        StudentId = studentId,
                        TaskId = taskId
                    });
        }

        //характеристическая функция (функция принадлежности)
        private double CalculateQuentifier(double x, double a, double b)
        {
            if (x < a)
                return 0;

            if (x >= a && x <= b)
                return (x - a) / (b - a);

            return 1;
        }

        private List<double> CalculateWeights(int n, double a, double b)
        {
            List<double> result = new List<double>();
            for (int i = 1; i <= n; i++)
            {
                result.Add(CalculateQuentifier((double)i / (double)n, a, b) - CalculateQuentifier((double)(i - 1) / (double)n, a, b));
            }

            return result;
        }

        private double GetOWA(List<double> weights, List<double?> preferences)
        {
            double result = 0;
            preferences.Sort();
            preferences.Reverse();
            for (int i = 0; i < preferences.Count; i++)
            {
                result += weights.ElementAt(i) * preferences.ElementAt(i).Value;
            }

            return result;
        }

        private double GetPhiNetFlow(double[,] preferences, int currentWork)
        {
            double sum1 = 0, sum2 = 0;
            for (int i = 0; i < preferences.GetLength(0); i++)
            {
                if (i != currentWork)
                {
                    sum1 += preferences[currentWork, i];
                    sum2 += preferences[i, currentWork];
                }
            }

            return sum1 - sum2;
        }

        private List<double> GetAbsoluteGrades(List<double> phis, int emin, int emax)
        {
            double PhiMin = phis.Min();
            double PhiMax = phis.Max();
            List<double> result = new List<double>();

            foreach (var phi in phis)
            {
                result.Add(((phi - PhiMin) * (emax - emin)) / (PhiMax - PhiMin) + emin);
            }
            return result;
        }

        public void CalculateOrdinalGrades(Entities.Task task)
        {
            List<Entities.OrdinalGrade> grades;
            using (IUnitOfWork unitOfWork = _unitOfWorkFactory.Create())
            {
                grades = GetOrdinalGradesByTask(task).ToList();

                unitOfWork.Commit();
            }

            //каждому студенту присваивается номер
            Dictionary<string, int> mapping = new Dictionary<string, int>();
            int countStudents = 0;
            foreach (var grade in grades)
            {
                if (!mapping.ContainsKey(grade.FirstStudent.Id))
                {
                    mapping.Add(grade.FirstStudent.Id, countStudents);
                    countStudents++;
                }
            }

            double a = 0.2, b = 0.8;

            double[,] preferences = new double[countStudents, countStudents];
            for (int i = 0; i < countStudents; i++)
            {
                for (int j = 0; j < countStudents; j++)
                {
                    //вычисляются веса (вес - это число от 0 до 1, которое характеризует вес i-ой оценки в порядке убывания)
                    var weights = CalculateWeights(grades.Count(grade => grade.FirstStudent.Id == mapping.First(x => x.Value == i).Key
                                                                         && grade.SecondStudent.Id == mapping.First(x => x.Value == j).Key), a, b);

                    if (i != j)
                    {
                        //вычисляется массив оценок (коэф-тов предпочтения) * массив весов для каждого студента
                        List<double?> individualFPR =
                            grades
                                .Where(
                                    grade => grade.FirstStudent.Id == mapping.First(x => x.Value == i).Key
                                             && grade.SecondStudent.Id == mapping.First(x => x.Value == j).Key)
                                .Select(grade => grade.Grade).ToList();
                        preferences[i, j] = GetOWA(weights, individualFPR);
                    }
                    else
                    {
                        preferences[i, j] = 0.5;
                    }

                }
            }

            List<double> resultGrades = new List<double>();

            for (int i = 0; i < countStudents; i++)
            {
                resultGrades.Add(GetPhiNetFlow(preferences, i));
            }

            //вычисление абсолютных оценок
            List<double> absoluteGrades = GetAbsoluteGrades(resultGrades, 0, (int)task.MoodleMaxGrade);

            using (IUnitOfWork unitOfWork = _unitOfWorkFactory.Create())
            {
                for (int i = 0; i < absoluteGrades.Count; i++)
                {
                    var user = _userEntityService.Get(mapping.ElementAt(i).Key);
                    var submission = _submissionService.GetUserSubmission(user.Nickname, task.Id);

                    submission.SummaryGrade = (int)Math.Round(absoluteGrades.ElementAt(i));

                    _submissionService.Update(submission);
                }

                unitOfWork.Commit();
            }
        }

        public IEnumerable<Entities.OrdinalGrade> GetOrdinalGradesByTask(Entities.Task task)
        {
            return _queryBuilder
                .For<IEnumerable<Entities.OrdinalGrade>>()
                .With(new GetOrdinalGradesByTaskCriterion()
                {
                    Task = task
                });
        }
    }
}
