﻿namespace Domain.Services.OrdinalGrade
{
    using System.Collections.Generic;

    public interface IOrdinalGradeService
    {
        IEnumerable<Entities.OrdinalGrade> GetOrdinalGradesByUsernameAndTask(string username, Entities.Task task);

        bool IsGradedSubmissionsByUser(string studentId, string taskId);

        void CalculateOrdinalGrades(Entities.Task task);

        IEnumerable<Entities.OrdinalGrade> GetOrdinalGradesByTask(Entities.Task task);
    }
}
