﻿namespace Domain.Services.Rubric
{
    using System.Collections.Generic;
    using Entities;

    public interface IRubricService
    {
        IEnumerable<RubricLevel> GetRubricLevelsByRubric(string rubricId);

        void Update(Rubric rubric);

        void DeleteRubricLevel(RubricLevel level);

        IEnumerable<Rubric> GetRubricByTask(string taskId);
    }
}
