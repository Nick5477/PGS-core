﻿namespace Domain.Services.Rubric
{
    using System.Collections.Generic;
    using Entities;
    using Queries;
    using Queries.Criterion.RubricLevel;
    using Repositories;
    using Commands;
    using Commands.Contexts.RubricLevel;
    using Queries.Criterion.Rubric;

    public class RubricService : IEntityService<Rubric>, IRubricService
    {
        private readonly IQueryBuilder _queryBuilder;
        private readonly IRepository<Rubric> _repository;
        private readonly ICommandBuilder _commandBuilder;

        public RubricService(
            IQueryBuilder queryBuilder,
            IRepository<Rubric> repository,
            ICommandBuilder commandBuilder)
        {
            _queryBuilder = queryBuilder;
            _repository = repository;
            _commandBuilder = commandBuilder;
        }

        public void Add(Rubric entity)
        {
            _repository.Add(entity);
        }

        public Rubric Get(string id)
        {
            return _repository.Get(id);
        }

        public void Delete(string id)
        {
            _repository.Delete(Get(id));
        }

        public IEnumerable<RubricLevel> GetRubricLevelsByRubric(string rubricId)
        {
            return _queryBuilder
                .For<IEnumerable<RubricLevel>>()
                .With(
                    new GetRubricLevelsByRubricCriterion()
                    {
                        RubricId = rubricId
                    });
        }

        public void Update(Rubric rubric)
        {
            _repository.Update(rubric);
        }

        public void DeleteRubricLevel(RubricLevel level)
        {
            _commandBuilder
                .Execute(new DeleteRubricLevelCommandContext()
                {
                    RubricLevel = level
                });
        }

        public IEnumerable<Rubric> GetRubricByTask(string taskId)
        {
            return _queryBuilder
                .For<IEnumerable<Rubric>>()
                .With(
                    new GetRubricsByTaskCriterion()
                    {
                        TaskId = taskId
                    });
        }
    }
}
