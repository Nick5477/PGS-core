﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Commands;
using Domain.Commands.Contexts.DistributionUnit;
using Domain.Queries;
using Domain.Queries.Criterion.DistributionUnit;

namespace Domain.Services.DistributionUnit
{
    public class DistributionService : IDistributionService
    {
        private readonly ICommandBuilder _commandBuilder;
        private readonly IQueryBuilder _queryBuilder;

        public DistributionService(
            ICommandBuilder commandBuilder,
            IQueryBuilder queryBuilder)
        {
            _commandBuilder = commandBuilder;
            _queryBuilder = queryBuilder;
        }

        private List<int>[] GetDistribution(int n, int k)
        {
            List<int>[] res = new List<int>[n];
            for (int i = 0; i < n; i++)
            {
                res[i] = new List<int>();
            }
            Random rnd = new Random();
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < k; j++)
                {
                    int target = rnd.Next(0, n);
                    int cnt = 0;
                    while (true)
                    {
                        if (!res[target].Contains(i)
                            && res[target].Count < k
                            && target != i)
                        {
                            res[target].Add(i);
                            break;
                        }
                        else
                        {
                            target = rnd.Next(0, n);
                        }
                        if (cnt > 100)
                        {
                            res = Swap(i, res, k);
                            cnt = 0;
                            break;
                        }
                        cnt++;
                    }
                }
            }

            return res;
        }

        List<int>[] Swap(int nRabota, List<int>[] mas, int k)
        {
            int nTroubleElement = -1;
            for (int i = 0; i < mas.Length; i++)
            {
                if (mas[i].Count < k)
                {
                    nTroubleElement = i;
                    break;
                }
            }

            for (int i = 0; i < mas.Length; i++)
            {
                if (!mas[i].Contains(nRabota))
                {
                    int? diff = FindDiff(mas[i], mas[nTroubleElement], nRabota);

                    if (diff.HasValue
                        && i != nRabota)
                    {
                        mas[i].Remove(diff.Value);
                        mas[nTroubleElement].Add(diff.Value);
                        mas[i].Add(nRabota);
                        return mas;
                    }
                }
            }
            return mas;
        }

        int? FindDiff(List<int> a, List<int> b, int nRabota)
        {
            return a.FirstOrDefault(x => !b.Contains(x) && x != nRabota);
        }

        public void CreateDistributions(int n, int k, Dictionary<int, string> users, string taskId)
        {
            var distributionArray = GetDistribution(n, k);
            List<Entities.DistributionUnit> distributions = new List<Entities.DistributionUnit>();
            for (int i = 0; i < n; i++)
            {
                int countWorks = 1;
                foreach (int studentId in distributionArray[i])
                {
                    var distribution = new Entities.DistributionUnit()
                    {
                        GraderId = users[i],
                        StudentId = users[studentId],
                        GraderWorkNumber = countWorks,
                        TaskId = taskId
                    };
                    distribution.InitializeId();

                    distributions.Add(distribution);

                    countWorks++;
                }
            }

            _commandBuilder.Execute(
                new AddDistributionUnitsCommandContext()
                {
                    Distributions = distributions
                });
        }

        public List<Entities.DistributionUnit> GetDistributionsByUsernameAndTask(string username, Entities.Task task)
        {
            return _queryBuilder
                .For<List<Entities.DistributionUnit>>()
                .With(
                    new GetDistributionsByUsernameAndTaskCriterion()
                    {
                        Username = username,
                        Task = task
                    });
        }

        public void DeleteDistributionUnitsByTaskId(string taskId)
        {
            _commandBuilder
                .Execute(new DeleteDistributionUnitsByTaskIdCommandContext()
                {
                    TaskId = taskId
                });
        }
    }
}
