﻿namespace Domain.Services.DistributionUnit
{
    using System.Collections.Generic;

    public interface IDistributionService
    {
        void CreateDistributions(int n, int k, Dictionary<int, string> users, string taskId);

        List<Entities.DistributionUnit> GetDistributionsByUsernameAndTask(string username, Entities.Task taskId);

        void DeleteDistributionUnitsByTaskId(string taskId);
    }
}
