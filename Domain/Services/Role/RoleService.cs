﻿namespace Domain.Services.Role
{
    using Repositories;

    public class RoleService : IEntityService<Entities.Role>
    {
        private readonly IRepository<Entities.Role> _repository;

        public RoleService(IRepository<Entities.Role> repository)
        {
            _repository = repository;
        }

        public void Add(Entities.Role entity)
        {
            throw new System.NotImplementedException();
        }

        public Entities.Role Get(string id)
        {
            return _repository.Get(id);
        }

        public void Delete(string id)
        {
            throw new System.NotImplementedException();
        }
    }
}
