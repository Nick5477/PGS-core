﻿namespace Domain.Services.Submission
{
    using System.Collections.Generic;

    using Domain.Entities;

    public interface ISubmissionService
    {
        Entities.Submission GetUserSubmission(string username, string taskid);

        List<Entities.Submission> GetSubmissionsByDistributions(List<Entities.DistributionUnit> distributions);

        List<Entities.Submission> GetSubmissionsByTask(Entities.Task task);

        void Update(Entities.Submission submission);

        bool LoadGradesToMoodle(Task task);
    }
}
