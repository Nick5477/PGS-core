﻿namespace Domain.Services.Submission
{
    using System;
    using System.Collections.Generic;

    using Domain.Commands;
    using Domain.Commands.Contexts.Submission;
    using Domain.Entities;
    using Domain.Queries;
    using Domain.Queries.Criterion.Submission;
    using Domain.Repositories;

    public class SubmissionService : IEntityService<Submission>, ISubmissionService
    {
        private readonly IQueryBuilder _queryBuilder;

        private readonly IRepository<Submission> _repository;

        private readonly ICommandBuilder _commandBuilder;

        public SubmissionService(
            IQueryBuilder queryBuilder,
            IRepository<Submission> repository,
            ICommandBuilder commandBuilder)
        {
            _queryBuilder = queryBuilder;
            _repository = repository;
            _commandBuilder = commandBuilder;
        }
        public void Add(Submission entity)
        {
            _repository.Add(entity);
        }

        public Submission Get(string id)
        {
            return _repository.Get(id);
        }

        public void Delete(string id)
        {
            throw new NotImplementedException();
        }

        public Submission GetUserSubmission(string username, string taskid)
        {
            return _queryBuilder
                .For<Submission>()
                .With(new GetUserSubmissionCriterion
                {
                    TaskId = taskid,
                    Username = username
                });
        }

        public List<Submission> GetSubmissionsByDistributions(List<DistributionUnit> distributions)
        {
            return _queryBuilder
                .For<List<Submission>>()
                .With(new GetSubmissionsByDistributionsCriterion
                {
                    Distributions = distributions
                });
        }

        public List<Submission> GetSubmissionsByTask(Task task)
        {
            return _queryBuilder
                .For<List<Submission>>()
                .With(new GetSubmissionsByTaskCriterion
                {
                    Task = task
                });
        }

        public void Update(Submission submission)
        {
            _repository.Update(submission);
        }

        public bool LoadGradesToMoodle(Task task)
        {
            try
            {
                _commandBuilder.Execute(
                    new LoadGradesToMoodleCommandContext()
                    {
                        Task = task
                    });
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }
    }
}
