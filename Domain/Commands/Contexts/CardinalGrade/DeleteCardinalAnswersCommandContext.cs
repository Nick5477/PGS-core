﻿namespace Domain.Commands.Contexts.CardinalGrade
{
    using Domain.Entities;

    public class DeleteCardinalAnswersCommandContext : ICommandContext
    {
        public CardinalGrade CardinalGrade { get; set; }
    }
}
