﻿namespace Domain.Commands.Contexts.Task
{
    public class AddTaskCommandContext : ICommandContext
    {
        public Entities.Task NewTask { get; set; }
    }
}
