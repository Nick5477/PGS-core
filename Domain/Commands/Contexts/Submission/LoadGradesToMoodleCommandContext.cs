﻿namespace Domain.Commands.Contexts.Submission
{
    using Domain.Entities;

    public class LoadGradesToMoodleCommandContext : ICommandContext
    {
        public Task Task { get; set; }
    }
}
