﻿namespace Domain.Commands.Contexts.DistributionUnit
{
    using System.Collections.Generic;

    public class AddDistributionUnitsCommandContext : ICommandContext
    {
        public List<Entities.DistributionUnit> Distributions { get; set; }
    }
}
