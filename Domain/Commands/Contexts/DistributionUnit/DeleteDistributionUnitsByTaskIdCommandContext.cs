﻿namespace Domain.Commands.Contexts.DistributionUnit
{
    public class DeleteDistributionUnitsByTaskIdCommandContext : ICommandContext
    {
        public string TaskId { get; set; }
    }
}
