﻿namespace Domain.Commands.Contexts.Course
{
    public class BindUserToCourseCommandContext : ICommandContext
    {
        public Entities.Course Course { get; set; } 

        public Entities.User User { get; set; }
    }
}
