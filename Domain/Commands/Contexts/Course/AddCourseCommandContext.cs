﻿namespace Domain.Commands.Contexts.Course
{
    public class AddCourseCommandContext : ICommandContext
    {
        public Entities.Course NewCourse { get; set; }
    }
}
