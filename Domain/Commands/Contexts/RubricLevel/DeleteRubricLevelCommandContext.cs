﻿namespace Domain.Commands.Contexts.RubricLevel
{
    public class DeleteRubricLevelCommandContext : ICommandContext
    {
        public Entities.RubricLevel RubricLevel { get; set; }
    }
}
