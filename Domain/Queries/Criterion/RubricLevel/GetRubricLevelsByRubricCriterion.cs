﻿namespace Domain.Queries.Criterion.RubricLevel
{
    public class GetRubricLevelsByRubricCriterion : ICriterion
    {
        public string RubricId { get; set; }
    }
}
