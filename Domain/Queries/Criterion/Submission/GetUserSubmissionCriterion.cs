﻿namespace Domain.Queries.Criterion.Submission
{
    public class GetUserSubmissionCriterion : ICriterion
    {
        public string Username { get; set; }

        public string TaskId { get; set; }
    }
}
