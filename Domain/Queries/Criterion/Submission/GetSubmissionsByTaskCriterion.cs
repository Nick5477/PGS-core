﻿namespace Domain.Queries.Criterion.Submission
{
    public class GetSubmissionsByTaskCriterion : ICriterion
    {
        public Entities.Task Task { get; set; }
    }
}
