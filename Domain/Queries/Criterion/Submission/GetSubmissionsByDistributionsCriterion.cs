﻿namespace Domain.Queries.Criterion.Submission
{
    using System.Collections.Generic;

    public class GetSubmissionsByDistributionsCriterion : ICriterion
    {
        public List<Entities.DistributionUnit> Distributions { get; set; }
    }
}
