﻿namespace Domain.Queries.Criterion.Rubric
{
    public class GetRubricsByTaskCriterion : ICriterion
    {
        public string TaskId { get; set; }
    }
}
