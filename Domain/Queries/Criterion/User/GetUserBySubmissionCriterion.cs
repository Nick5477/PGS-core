﻿namespace Domain.Queries.Criterion.User
{
    using Domain.Entities;

    public class GetUserBySubmissionCriterion : ICriterion
    {
        public Submission Submission { get; set; }
    }
}
