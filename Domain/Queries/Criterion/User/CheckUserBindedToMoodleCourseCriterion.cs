﻿namespace Domain.Queries.Criterion.User
{
    public class CheckUserBindedToMoodleCourseCriterion : ICriterion
    {
        public string Username { get; set; }

        public int MoodleId { get; set; }
    }
}
