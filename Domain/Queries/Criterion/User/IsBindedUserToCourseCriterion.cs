﻿namespace Domain.Queries.Criterion.User
{
    public class IsBindedUserToCourseCriterion : ICriterion
    {
        public string UserId { get; set; }

        public string CourseId { get; set; }
    }
}
