﻿namespace Domain.Queries.Criterion.User
{
    public class GetUsersByCourseCriterion : ICriterion
    {
        public string CourseId { get; set; }
    }
}
