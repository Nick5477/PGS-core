﻿namespace Domain.Queries.Criterion.User
{
    public class GetUserByNameCriterion : ICriterion
    {
        public string Username { get; set; }
    }
}
