﻿namespace Domain.Queries.Criterion.DistributionUnit
{
    public class GetDistributionsByUsernameAndTaskCriterion : ICriterion
    {
        public string Username { get; set; }

        public Entities.Task Task { get; set; }
    }
}
