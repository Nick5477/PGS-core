﻿namespace Domain.Queries.Criterion.Task
{
    public class GetTaskByMoodleIdCriterion : ICriterion
    {
        public long TaskMoodleId { get; set; }
    }
}
