﻿namespace Domain.Queries.Criterion.Task
{
    public class GetTaskByCourseIdCriterion : ICriterion
    {
        public string CourseId { get; set; }
    }
}
