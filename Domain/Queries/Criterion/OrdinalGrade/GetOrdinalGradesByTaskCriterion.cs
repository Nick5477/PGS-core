﻿namespace Domain.Queries.Criterion.OrdinalGrade
{
    public class GetOrdinalGradesByTaskCriterion : ICriterion
    {
        public Entities.Task Task { get; set; }
    }
}
