﻿namespace Domain.Queries.Criterion.OrdinalGrade
{
    public class GetOrdinalGradesByUsernameAndTaskCriterion : ICriterion
    {
        public string Username { get; set; }

        public Entities.Task Task { get; set; }
    }
}
