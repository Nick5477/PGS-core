﻿namespace Domain.Queries.Criterion.OrdinalGrade
{
    public class IsGradedSubmissionsByUserCriterion : ICriterion
    {
        public string StudentId { get; set; }

        public string TaskId { get; set; }
    }
}
