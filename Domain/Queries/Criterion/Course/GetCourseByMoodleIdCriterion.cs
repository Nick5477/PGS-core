﻿namespace Domain.Queries.Criterion.Course
{
    public class GetCourseByMoodleIdCriterion : ICriterion
    {
        public long MoodleId { get; set; }
    }
}
