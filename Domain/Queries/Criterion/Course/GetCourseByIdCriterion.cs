﻿namespace Domain.Queries.Criterion.Course
{
    public class GetCourseByIdCriterion : ICriterion
    {
        public string CourseId { get; set; }
    }
}
