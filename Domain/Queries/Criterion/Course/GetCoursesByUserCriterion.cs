﻿namespace Domain.Queries.Criterion.Course
{
    public class GetCoursesByUserCriterion : ICriterion
    {
        public string Username { get; set; }
    }
}
