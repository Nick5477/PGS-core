﻿namespace Domain.Queries.Criterion.CardinalGrade
{
    public class GetCardinalGradeBySubmissionAndUserCriterion : ICriterion
    {
        public Entities.Submission Submission { get; set; }

        public Entities.User User { get; set; }
    }
}
