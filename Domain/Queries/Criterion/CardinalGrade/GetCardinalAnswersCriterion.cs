﻿namespace Domain.Queries.Criterion.CardinalGrade
{
    public class GetCardinalAnswersCriterion : ICriterion
    {
        public Entities.CardinalGrade CardinalGrade { get; set; }
    }
}
