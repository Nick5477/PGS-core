﻿namespace Domain.Queries.Criterion.CardinalGrade
{
    public class GetCardinalGradesByTaskCriterion : ICriterion
    {
        public Entities.Task Task { get; set; }
    }
}
