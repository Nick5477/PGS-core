﻿namespace Domain.Queries.Criterion.CardinalGrade
{
    public class GetCardinalGradesByTaskAndUserCriterion : ICriterion
    {
        public Entities.Task Task { get; set; }

        public Entities.User User { get; set; }
    }
}
