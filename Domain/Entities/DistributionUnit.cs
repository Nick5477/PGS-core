﻿namespace Domain.Entities
{
    using System;

    public class DistributionUnit : IEntity
    {
        public string Id { get; private set; }

        public string GraderId { get; set; }

        public string StudentId { get; set; }

        public string TaskId { get; set; }

        public int GraderWorkNumber { get; set; }

        public DistributionUnit(){}

        public void InitializeId()
        {
            if (Id == null)
                Id = Guid.NewGuid().ToString();
        }
    }
}
