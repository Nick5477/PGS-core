﻿namespace Domain.Entities
{
    using System;

    public class RubricLevel : IEntity
    {
        public string Id { get; private set; }

        public string Description { get; set; }

        public int Points { get; set; }

        public RubricLevel()
        {
            
        }

        public void InitializeId()
        {
            if (Id == null)
                Id = Guid.NewGuid().ToString();
        }
    }
}
