﻿namespace Domain.Entities
{
    using System;
    using System.Collections.Generic;

    public class CardinalGrade : IEntity
    {
        public string Id { get; set; }

        public User Grader { get; set; }

        public Submission Submission { get; set; }

        public int Grade { get; set; }

        public Dictionary<Rubric, RubricLevel> Answers { get; set; }

        public void InitializeId()
        {
            if (Id == null)
                Id = Guid.NewGuid().ToString();
        }

        public CardinalGrade()
        {
            
        }

        public CardinalGrade(
            User grader,
            Submission submission,
            int grade,
            Dictionary<Rubric, RubricLevel> answers)
        {
            Grader = grader;
            Submission = submission;
            Grade = grade;
            Answers = answers;
        }

        public CardinalGrade(
            User grader,
            Submission submission)
        {
            Grader = grader;
            Submission = submission;
        }
    }
}
