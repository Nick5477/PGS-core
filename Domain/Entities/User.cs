﻿namespace Domain.Entities
{
    using System;
    using System.Collections.Generic;

    public class User : IEntity
    {
        public string Id { get; private set; }

        public string Nickname { get; set; }

        public Role Role { get; set; }

        public List<Course> MyCourses { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        public string Patronymic { get; set; }

        public string FIO => LastName + Name + Patronymic;

        public int MoodleId { get; set; }

        [Obsolete("Only for reflection", true)]
        public User(){}

        public User(string nickname, Role role)
        {
            if (string.IsNullOrWhiteSpace(nickname))
                throw new ArgumentNullException("Недопустимый никнейм!");

            Nickname = nickname;
            Role = role;
        }

        public void InitializeId()
        {
            if (Id == null)
                Id = Guid.NewGuid().ToString();
        }
    }
}
