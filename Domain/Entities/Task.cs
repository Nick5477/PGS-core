﻿namespace Domain.Entities
{
    using System;

    public enum PeerGradingEnum
    {
        Cardinal = 0,
        Ordinal = 1
    }

    public class Task : IEntity
    {
        private string _description;

        private string _cardinalGradingDescription;

        public string Id { get; set; }

        public long MoodleId { get; set; }

        public string Name { get; set; }

        public string Description
        {
            get => string.IsNullOrWhiteSpace(_description) ? "<отсутствует>" : _description;
            set { _description = value; }
        }

        public DateTime AllowSubmissionFromDate { get; set; }

        public DateTime DueDate { get; set; }

        public DateTime? GradingDate { get; set; }

        public bool IsClosed => DueDate < DateTime.UtcNow;

        public long MaxGrade { get; set; }

        public Course Course { get; set; }

        public bool? IsDistributed { get; set; }

        public int? MaxWorks { get; set; }

        public PeerGradingEnum PeerGrading { get; set; }

        public Int64 MoodleMaxGrade { get; set; }

        public string CardinalGradingDescription
        {
            get => string.IsNullOrWhiteSpace(_cardinalGradingDescription) ? "<отсутствует>" : _cardinalGradingDescription;
            set { _cardinalGradingDescription = value; }
        }
    

        public Task() { }

        public Task(
            long moodleId,
            string name,
            string description,
            DateTime allowSubmissionFromDate,
            DateTime dueDate,
            int moodleMaxGrade,
            DateTime gradingDate,
            int maxWorks,
            PeerGradingEnum peerGrading)
        {
            MoodleId = moodleId;
            Name = name;
            Description = description;
            AllowSubmissionFromDate = allowSubmissionFromDate;
            DueDate = dueDate;
            MoodleMaxGrade = moodleMaxGrade;
            GradingDate = gradingDate;
            MaxWorks = maxWorks;
            PeerGrading = peerGrading;
        }

        public Task(
            string id,
            long moodleId,
            string name,
            string description,
            string allowSubmissionFromDate,
            string dueDate,
            int moodleMaxGrade)
        {
            Id = id;
            MoodleId = moodleId;
            Name = name;
            Description = description;
            AllowSubmissionFromDate = DateTime.Parse(allowSubmissionFromDate);
            DueDate = DateTime.Parse(dueDate);
            MoodleMaxGrade = moodleMaxGrade;
        }

        public void InitializeId()
        {
            if (Id == null)
                Id = Guid.NewGuid().ToString();
        }

        public void SetCourse(Course course)
        {
            Course = course ?? throw new ArgumentNullException(nameof(course));
        }
    }
}
