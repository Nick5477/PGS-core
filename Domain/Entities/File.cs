﻿namespace Domain.Entities
{
    using System;

    public class File : IEntity
    {
        public const int MaxLengthBytes = 2 * 1024 * 1024;

        public string Id { get; private set; }

        public string FileName { get; set; } 

        public string Path { get; set; }

        public File() { }

        public File(string path, string fileName)
        {
            string extension = fileName.Substring(fileName.LastIndexOf('.'));
            fileName = $"{Guid.NewGuid().ToString()}{extension}";
            Id = fileName;
            FileName = fileName;
            Path = $"{path}\\{fileName}";
        }
    }
}
