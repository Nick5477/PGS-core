﻿namespace Domain.Entities
{
    using System;

    public class Role : IEntity
    {

        public class Keys
        {
            public static string Teacher { get; } = "c328e9c5-6bc1-42f0-ac93-9ba3eeeacc0f";

            public static string Student { get; } = "5887d940-9a36-4c30-b3c6-33558749e8a6";

            public static string Admin { get; } = "92bdb064-2bd0-4cb1-8082-c9b20844d3ce";
        }

        public string Id { get; set; }

        public string Name { get; set; }

        [Obsolete("Only for reflection", true)]
        public Role() { }

        public void InitializeId()
        {
            if (Id == null)
                Id = Guid.NewGuid().ToString();
        }
    }
}
