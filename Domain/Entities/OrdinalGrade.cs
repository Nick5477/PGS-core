﻿namespace Domain.Entities
{
    using System;

    public class OrdinalGrade : IEntity
    {
        public string Id { get; private set; }

        public Task Task { get; set; }

        public User Grader { get; set; }

        public User FirstStudent { get; set; }

        public User SecondStudent { get; set; }

        public double? Grade { get; set; }

        public OrdinalGrade(){}

        public OrdinalGrade(Task task, User grader, User firstStudent, User secondStudent, int grade)
        {
            Task = task;
            Grader = grader;
            FirstStudent = firstStudent;
            SecondStudent = secondStudent;
            Grade = grade;
        }

        public void InitializeId()
        {
            if (Id == null)
                Id = Guid.NewGuid().ToString();
        }
    }
}
