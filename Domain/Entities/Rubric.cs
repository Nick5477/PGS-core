﻿namespace Domain.Entities
{
    using System.Collections.Generic;
    using System;

    public class Rubric : IEntity
    {
        public string Id { get; private set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public Task Task { get; set; }

        public List<RubricLevel> Levels { get; set; }

        public Rubric()
        {
            Levels = new List<RubricLevel>();
        }

        public Rubric(
            string id,
            string name,
            string description,
            Task task)
        {
            Id = id;
            Name = name;
            Description = description;
            Task = task;
            Levels = new List<RubricLevel>();
        }

        public void InitializeId()
        {
            if (Id == null)
                Id = Guid.NewGuid().ToString();
        }
    }
}
