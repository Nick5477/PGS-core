﻿namespace Domain.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class Submission : IEntity
    {
        private string _textAnswer;

        public string Id { get; private set; }

        public Task Task { get; set; }

        public string TextAnswer
        {
            get => string.IsNullOrWhiteSpace(_textAnswer) ? "<отсутствует>" : _textAnswer;
            set { _textAnswer = value; }
        }

        public File FileAnswer { get; set; }

        public DateTime UploadTime { get; set; }

        public int Priority { get; set; }

        public List<User> Graders { get; set; }

        public Dictionary<User, int?> Grades { get; set; }

        public int SummaryGrade { get; set; }

        public User User { get; set; }

        public Submission()
        {
            
        }

        public Submission(
            Task task,
            string textAnswer,
            File fileAnswer,
            DateTime uploadTime,
            int priority,
            List<User> graders,
            Dictionary<User, int?> grades,
            int summaryGrade,
            User user)
        {
            Task = task;
            TextAnswer = textAnswer;
            FileAnswer = fileAnswer;
            UploadTime = uploadTime;
            Priority = priority;
            Graders = graders;
            Grades = grades;
            SummaryGrade = summaryGrade;
            User = user;
        }

        public Submission(
            Task task,
            string textAnswer,
            File fileAnswer,
            DateTime uploadTime,
            User user)
        {
            Task = task;
            TextAnswer = textAnswer;
            FileAnswer = fileAnswer;
            UploadTime = uploadTime;
            User = user;
            SetPriority();
        }

        public bool IsGradedByUser(string username)
        {
            return Grades.Keys.Any(u => u.Nickname == username);
        }

        public void SetPriority()
        {
            if ((double) (Task.DueDate - UploadTime).Ticks /
                (double) (Task.DueDate - Task.AllowSubmissionFromDate).Ticks <= 0.5)
            {
                Priority = 1;
            }
            if ((double) (Task.DueDate - UploadTime).Ticks /
                (double) (Task.DueDate - Task.AllowSubmissionFromDate).Ticks <= 0.8)
            {
                Priority = 2;
            }
            else
            {
                Priority = 3;
            }
        }

        public void InitializeId()
        {
            if (Id == null)
                Id = Guid.NewGuid().ToString();
        }
    }
}
