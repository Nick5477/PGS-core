﻿namespace Domain.Entities
{
    using System;

    public class Course : IEntity
    {
        public string Id { get; private set; }

        public int MoodleId { get; set; }

        public string FullName { get; set; }

        public string ShortName { get; set; }

        [Obsolete("Only for reflection", true)]
        public Course(){}

        public Course(int moodleId, string fullName, string shortName)
        {
            if (moodleId<=0)
                throw new ArgumentOutOfRangeException("MoodleId не может быть <= 0");
            if (string.IsNullOrWhiteSpace(fullName))
                throw new ArgumentNullException(nameof(fullName));

            MoodleId = moodleId;
            FullName = fullName;
            ShortName = shortName;
        }

        public void InitializeId()
        {
            if (Id == null)
                Id = Guid.NewGuid().ToString();
        }
    }
}
