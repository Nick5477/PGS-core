--
-- ���� ������������ � ������� SQLiteStudio v3.1.1 � �� ��� 10 18:04:04 2018
--
-- �������������� ��������� ������: System
--
BEGIN TRANSACTION;

-- �������: Course
CREATE TABLE Course (Id VARCHAR (32) PRIMARY KEY NOT NULL , MoodleId INT, FullName VARCHAR (40), ShortName VARCHAR (30));

-- �������: Task
CREATE TABLE Task (Id VARCHAR (32) PRIMARY KEY  NOT NULL, MoodleId INT, Name VARCHAR (30), Description VARCHAR (500), AllowSubmissionFromDate DATETIME, DueDate DATETIME, GradingDate DATETIME, MaxGrade INT, CourseId VARCHAR (32) REFERENCES Course (Id), IsDistributed bit, MaxWorks INTEGER, PeerGrading INT, CardinalGradingDescription NVARCHAR, MoodleMaxGrade INTEGER);

-- �������: Role
CREATE TABLE [Role] (Id NVARCHAR PRIMARY KEY  NOT NULL, Name NVARCHAR);

-- �������: User
CREATE TABLE [User] (Id VARCHAR (32) PRIMARY KEY  NOT NULL, Nickname VARCHAR (30), RoleId NVARCHAR REFERENCES [Role] (Id), Name NVARCHAR, LastName NVARCHAR, Patronymic NVARCHAR, MoodleId INTEGER);

-- �������: File
CREATE TABLE [File] (Id NVARCHAR PRIMARY KEY, FileName NVARCHAR NOT NULL, Path VARCHAR (500));

-- �������: Submission
CREATE TABLE Submission (Id VARCHAR (32) PRIMARY KEY  NOT NULL, TextAnswer VARCHAR (5000), FileAnswer NVARCHAR REFERENCES [File] (Id), SummaryGrade INT, Priority INTEGER, TaskId VARCHAR (32) REFERENCES Task (Id), UserId VARCHAR (32) REFERENCES [User] (Id), UploadTime DATETIME);

-- �������: Rubric
CREATE TABLE Rubric (Id VARCHAR (32) PRIMARY KEY  NOT NULL, Name VARCHAR(30), Description VARCHAR (500), TaskId VARCHAR (32) REFERENCES Task (Id));

-- �������: RubricLevel
CREATE TABLE RubricLevel (Id VARCHAR (32) PRIMARY KEY  NOT NULL, Description VARCHAR (50), Points INT, RubricId VARCHAR (32) REFERENCES Rubric (Id));

-- �������: CardinalGrade
CREATE TABLE CardinalGrade (Id VARCHAR (32) PRIMARY KEY  NOT NULL, GraderId VARCHAR (32) REFERENCES [User] (Id), SubmissionId VARCHAR (32) REFERENCES Submission (Id), Grade INT);

-- �������: CardinalGradeRubric
CREATE TABLE CardinalGradeRubric (Id INTEGER PRIMARY KEY IDENTITY(1,1) NOT NULL , CardinalGradeId VARCHAR (32) REFERENCES CardinalGrade (Id), RubricId VARCHAR (32) REFERENCES Rubric (Id), RubricLevelId VARCHAR (32) REFERENCES RubricLevel (Id));

-- �������: Distribution
CREATE TABLE [Distribution] (Id VARCHAR (32) PRIMARY KEY  NOT NULL, GraderId VARCHAR (32) REFERENCES [User] (Id), StudentId VARCHAR (32) REFERENCES [User] (Id), TaskId VARCHAR (32) REFERENCES Task (Id), GraderWorkNumber INTEGER);

-- �������: OrdinalGrade
CREATE TABLE OrdinalGrade (Id VARCHAR (32) PRIMARY KEY  NOT NULL, idTask VARCHAR (32) REFERENCES Task (Id), idGrader VARCHAR (32) REFERENCES [User] (Id), idStudent1 VARCHAR (32) REFERENCES [User] (Id), idStudent2 VARCHAR (32) REFERENCES [User] (Id), Grade FLOAT);

-- �������: UserCourse
CREATE TABLE UserCourse (Id INTEGER PRIMARY KEY IDENTITY(1,1)  NOT NULL, UserId VARCHAR (32) REFERENCES [User] (Id), CourseId VARCHAR (32) REFERENCES Course (Id));

COMMIT TRANSACTION;
