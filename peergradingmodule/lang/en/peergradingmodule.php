<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * English strings for peergradingmodule
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_peergradingmodule
 * @copyright  2016 Your Name <your@email.address>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['modulename'] = 'Peer Grading Module';
$string['modulenameplural'] = 'peergradingmodules';
$string['modulename_help'] = 'Данный элемент курса предназначен для добавления заданий с использованием коллегиального оценивания';
$string['peergradingmodule:addinstance'] = 'Добавить новое задание с коллегиальным оцениванием';
$string['peergradingmodule:submit'] = 'Submit peergradingmodule';
$string['peergradingmodule:view'] = 'View peergradingmodule';
$string['peergradingmodulefieldset'] = 'Custom example fieldset';
$string['peergradingmodulename'] = 'peergradingmodule name';
$string['peergradingmodulename_help'] = 'This is the content of the help tooltip associated with the peergradingmodulename field. Markdown syntax is supported.';
$string['peergradingmodule'] = 'peergradingmodule';
$string['pluginadministration'] = 'Администрирование Peer Grading Module';
$string['pluginname'] = 'peergradingmodule';
$string['gradingdate']= 'Оценить до';
$string['peergrading']= 'Выберите метод коллегиального оценивания';
$string['peergradingsection'] = 'Коллегиальное оценивание';
$string['maxworks'] = 'Количество работ, распределяемых каждому студенту';

