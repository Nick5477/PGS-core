﻿namespace Infrastructure.Db.SqlServer
{
    using System.Data;
    using System.Data.SqlClient;

    using Infrastructure.Db.Connections;

    public class MSSQLConnectionFactory : DbConnectionFactoryBase
    {
        public MSSQLConnectionFactory(string connectionString) : base(connectionString) { }

        protected override IDbConnection CreateConnection(string connectionString)
        {
            return new SqlConnection(connectionString);
        }
    }
}
