﻿namespace Infrastructure.Db.SqlServer
{
    using System.Data;
    using System.Data.SqlClient;

    using Infrastructure.Db.Commands;
    using Infrastructure.Db.Transactions;

    public class MSSQLCommandFactory : DbCommandFactoryBase
    {
        public MSSQLCommandFactory(IDbTransactionProvider transactionProvider) : base(transactionProvider) { }

        protected override IDbCommand CreateCommand()
        {
            return new SqlCommand()
            {
                Transaction = (SqlTransaction)_transactionProvider.CurrentTransaction
            };
        }
    }
}
