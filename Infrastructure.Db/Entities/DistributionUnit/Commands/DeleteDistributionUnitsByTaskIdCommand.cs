﻿namespace Infrastructure.Db.Entities.DistributionUnit.Commands
{
    using Domain.Commands;
    using Domain.Commands.Contexts.DistributionUnit;
    using Transactions;
    using Dapper;

    public class DeleteDistributionUnitsByTaskIdCommand : ICommand<DeleteDistributionUnitsByTaskIdCommandContext>
    {
        private readonly IDbTransactionProvider _transactionProvider;

        public DeleteDistributionUnitsByTaskIdCommand(IDbTransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider;
        }

        public void Execute(DeleteDistributionUnitsByTaskIdCommandContext commandContext)
        {
            _transactionProvider
                .CurrentTransaction
                .Connection
                .Execute(@"
                DELETE
                FROM Distribution
                WHERE TaskId=@task",
                new
                {
                    task = commandContext.TaskId
                });
        }
    }
}
