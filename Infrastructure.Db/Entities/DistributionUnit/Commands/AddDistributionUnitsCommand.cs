﻿namespace Infrastructure.Db.Entities.DistributionUnit.Commands
{
    using Dapper;
    using Domain.Commands;
    using Domain.Commands.Contexts.DistributionUnit;
    using Infrastructure.Db.Transactions;

    class AddDistributionUnitsCommand : ICommand<AddDistributionUnitsCommandContext>
    {
        private readonly IDbTransactionProvider _transactionProvider;

        public AddDistributionUnitsCommand(IDbTransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider;
        }
        public void Execute(AddDistributionUnitsCommandContext commandContext)
        {
            _transactionProvider
                .CurrentTransaction
                .Connection
                .Execute(@"
                    INSERT INTO Distribution
                    VALUES
                    (
                        @Id,
                        @GraderId,
                        @StudentId,
                        @TaskId,
                        @GraderWorkNumber
                    )",
                    commandContext.Distributions);
        }
    }
}
