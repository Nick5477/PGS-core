﻿namespace Infrastructure.Db.Entities.DistributionUnit.Queries
{
    using System.Collections.Generic;
    using System.Linq;
    using Dapper;
    using Domain.Queries;
    using Domain.Queries.Criterion.DistributionUnit;
    using Transactions;

    public class GetDistributionsByUsernameAndTask : IQuery<GetDistributionsByUsernameAndTaskCriterion, List<Domain.Entities.DistributionUnit>>
    {
        private readonly IDbTransactionProvider _transactionProvider;

        public GetDistributionsByUsernameAndTask(IDbTransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider;
        }

        public List<Domain.Entities.DistributionUnit> Ask(GetDistributionsByUsernameAndTaskCriterion criterion)
        {
            return _transactionProvider
                .CurrentTransaction
                .Connection
                .Query<Domain.Entities.DistributionUnit>(@"
                SELECT
                    d.Id,
                    d.GraderId,
                    d.StudentId,
                    d.TaskId,
                    d.GraderWorkNumber
                FROM
                    Distribution d
                    JOIN User u ON u.Id=d.GraderId
                WHERE
                    TaskId=@task AND u.Nickname=@grader
                ORDER BY d.GraderWorkNumber",
                    new
                    {
                        task = criterion.Task.Id,
                        grader = criterion.Username
                    }).ToList();
        }
    }
}
