﻿namespace Infrastructure.Db.Entities.Course.Commands
{
    using System;
    using Dapper;
    using Domain.Commands;
    using Domain.Commands.Contexts.Course;
    using Infrastructure.Db.Transactions;

    public class BindUserToCourseCommand : ICommand<BindUserToCourseCommandContext>
    {
        private readonly IDbTransactionProvider _transactionProvider;

        public BindUserToCourseCommand(IDbTransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider ?? throw new ArgumentNullException(nameof(transactionProvider));
        }
        public void Execute(BindUserToCourseCommandContext commandContext)
        {
            _transactionProvider
                .CurrentTransaction
                .Connection
                .Execute(@"
                INSERT INTO UserCourse
                (
                    UserId,
                    CourseId
                )
                VALUES
                (
                    @userid,
                    @courseid
                );", new
                {
                    userid=commandContext.User.Id,
                    courseid=commandContext.Course.Id
                });
        }
    }
}
