﻿namespace Infrastructure.Db.Entities.Course.Commands
{
    using System;
    using Domain.Commands;
    using Domain.Commands.Contexts.Course;
    using Domain.Services;

    public class AddCourseCommand : ICommand<AddCourseCommandContext>
    {
        private readonly IEntityService<Domain.Entities.Course> _service;

        public AddCourseCommand(IEntityService<Domain.Entities.Course> service)
        {
            _service = service ?? throw new ArgumentNullException(nameof(service));
        }
        public void Execute(AddCourseCommandContext commandContext)
        {
            _service.Add(commandContext.NewCourse);
        }
    }
}
