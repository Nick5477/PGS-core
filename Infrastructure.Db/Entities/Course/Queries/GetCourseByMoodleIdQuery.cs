﻿namespace Infrastructure.Db.Entities.Course.Queries
{
    using System;
    using System.Linq;

    using Dapper;

    using Domain.Queries;
    using Domain.Queries.Criterion.Course;

    using Infrastructure.Db.Transactions;

    class GetCourseByMoodleIdQuery : IQuery<GetCourseByMoodleIdCriterion, Domain.Entities.Course>
    {
        private readonly IDbTransactionProvider _transactionProvider;

        public GetCourseByMoodleIdQuery(IDbTransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider ?? throw new ArgumentNullException(nameof(transactionProvider));
        }
        public Domain.Entities.Course Ask(GetCourseByMoodleIdCriterion criterion)
        {
            return _transactionProvider
                .CurrentTransaction.Connection
                .Query<Domain.Entities.Course>(@"
                SELECT
                    Id,
                    MoodleId,
                    FullName,
                    ShortName
                FROM Course
                WHERE MoodleId=@moodleid;", new { moodleid = criterion.MoodleId })
                .FirstOrDefault();
        }
    }
}
