﻿namespace Infrastructure.Db.Entities.Course.Queries
{
    using System.Collections.Generic;
    using Domain.Queries;
    using Domain.Queries.Criterion.Course;
    using Infrastructure.Db.Mysql;
    using Infrastructure.Db.Postgres;

    using MySql.Data.MySqlClient;

    using Npgsql;

    public class SyncCoursesQuery : IQuery<SyncCoursesCriterion, IEnumerable<Domain.Entities.Course>>
    {
        public IEnumerable<Domain.Entities.Course> Ask(SyncCoursesCriterion criterion)
        {
            //List<Domain.Entities.Course> courses = new List<Domain.Entities.Course>();
            //using (MySqlConnection connection = MysqlConnectionProvider.CreateConnection())
            //{
            //    connection.Open();
            //    string moodleQuery =
            //        @"SELECT DISTINCT pg.course, c.fullname, c.shortname
            //          FROM moodle.mdl_peergradingmodule pg
            //          JOIN moodle.mdl_course c on pg.course=c.id";
            //    MySqlCommand command = new MySqlCommand(moodleQuery, connection);

            //    var reader = command.ExecuteReader();

            //    while (reader.Read())
            //    {
            //        courses.Add(new Domain.Entities.Course(
            //            reader.GetInt32("course"),
            //            reader.GetString("fullname"),
            //            reader.GetString("shortname")));
            //    }

            //}
            //return courses;

            List<Domain.Entities.Course> courses = new List<Domain.Entities.Course>();
            using (NpgsqlConnection connection = PostgresConnectionProvider.CreateConnection())
            {
                connection.Open();
                string moodleQuery =
                    @"SELECT DISTINCT pg.course, c.fullname, c.shortname
                      FROM mdl_peergradingmodule pg
                      JOIN mdl_course c on pg.course=c.id";
                NpgsqlCommand command = new NpgsqlCommand(moodleQuery, connection);

                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    courses.Add(new Domain.Entities.Course(
                        reader.GetInt32(0),
                        reader.GetString(1),
                        reader.GetString(2)));
                }

            }
            return courses;
        }
    }
}
