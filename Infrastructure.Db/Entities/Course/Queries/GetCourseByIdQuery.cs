﻿namespace Infrastructure.Db.Entities.Course.Queries
{
    using Domain.Queries;
    using Domain.Queries.Criterion.Course;
    using Domain.Services;

    class GetCourseByIdQuery : IQuery<GetCourseByIdCriterion, Domain.Entities.Course>
    {
        private readonly IEntityService<Domain.Entities.Course> _service;

        public GetCourseByIdQuery(IEntityService<Domain.Entities.Course> service)
        {
            _service = service;
        }
        public Domain.Entities.Course Ask(GetCourseByIdCriterion criterion)
        {
            return _service.Get(criterion.CourseId);
        }
    }
}
