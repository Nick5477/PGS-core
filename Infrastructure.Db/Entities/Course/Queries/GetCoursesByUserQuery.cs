﻿namespace Infrastructure.Db.Entities.Course.Queries
{
    using System.Collections.Generic;
    using Dapper;
    using Domain.Queries;
    using Domain.Queries.Criterion.Course;
    using Infrastructure.Db.Transactions;

    class GetCoursesByUserQuery : IQuery<GetCoursesByUserCriterion, IEnumerable<Domain.Entities.Course>>
    {
        private readonly IDbTransactionProvider _transactionProvider;

        public GetCoursesByUserQuery(IDbTransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider;
        }
        public IEnumerable<Domain.Entities.Course> Ask(GetCoursesByUserCriterion criterion)
        {
            return
                _transactionProvider
                    .CurrentTransaction
                    .Connection
                    .Query<Domain.Entities.Course>(@"
                    SELECT
                        c.Id,
                        c.MoodleId,
                        c.FullName,
                        c.ShortName
                    FROM Course c
                    JOIN UserCourse uc on c.Id=uc.CourseId
                    JOIN User u on u.Id=uc.UserId
                    WHERE u.Nickname=@nickname", new
                    {
                        nickname = criterion.Username
                    });
        }
    }
}
