﻿namespace Infrastructure.Db.Entities.Course.Queries
{
    using System.Collections.Generic;
    using Domain.Queries;
    using Domain.Queries.Criterion.Course;
    using Domain.Repositories;

    public class GetAllCoursesQuery : IQuery<GetAllCoursesCriterion,IEnumerable<Domain.Entities.Course>>
    {
        private readonly IRepository<Domain.Entities.Course> _repository;

        public GetAllCoursesQuery(IRepository<Domain.Entities.Course> repository)
        {
            _repository = repository;
        }
        public IEnumerable<Domain.Entities.Course> Ask(GetAllCoursesCriterion criterion)
        {
            return _repository.All();
        }
    }
}
