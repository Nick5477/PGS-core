﻿namespace Infrastructure.Db.Entities.Rubric.Queries
{
    using System.Collections.Generic;
    using Domain.Queries;
    using Domain.Queries.Criterion.Rubric;
    using Transactions;
    using Dapper;
    using Domain.Services.Rubric;
    using System.Linq;

    public class GetRubricsByTaskQuery : IQuery<GetRubricsByTaskCriterion, IEnumerable<Domain.Entities.Rubric>>
    {
        private readonly IDbTransactionProvider _transactionProvider;
        private readonly IRubricService _rubricService;

        public GetRubricsByTaskQuery(
            IDbTransactionProvider transactionProvider,
            IRubricService rubricService)
        {
            _transactionProvider = transactionProvider;
            _rubricService = rubricService;
        }

        public IEnumerable<Domain.Entities.Rubric> Ask(GetRubricsByTaskCriterion criterion)
        {
            var rubrics = _transactionProvider
                .CurrentTransaction
                .Connection
                .Query<Domain.Entities.Rubric>(@"
                    SELECT
                        Id,
                        Name,
                        Description
                    FROM
                        Rubric r
                    WHERE 
                        TaskId = @taskid",
                    new
                    {
                        taskid = criterion.TaskId
                    });

            foreach (var rubric in rubrics)
            {
                rubric.Levels = _rubricService.GetRubricLevelsByRubric(rubric.Id).ToList();
            }

            return rubrics;
        }
    }
}
