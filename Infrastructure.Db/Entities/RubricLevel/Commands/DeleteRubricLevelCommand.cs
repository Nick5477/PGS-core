﻿namespace Infrastructure.Db.Entities.RubricLevel.Commands
{
    using Domain.Commands;
    using Domain.Commands.Contexts.RubricLevel;
    using Transactions;
    using Dapper;

    public class DeleteRubricLevelCommand : ICommand<DeleteRubricLevelCommandContext>
    {
        private readonly IDbTransactionProvider _transactionProvider;

        public DeleteRubricLevelCommand(
            IDbTransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider;
        }

        public void Execute(DeleteRubricLevelCommandContext commandContext)
        {
            _transactionProvider
                .CurrentTransaction
                .Connection
                .Execute(@"
                    DELETE FROM RubricLevel
                    WHERE Id = @id",
                    new
                    {
                        id = commandContext.RubricLevel.Id
                    });
        }
    }
}
