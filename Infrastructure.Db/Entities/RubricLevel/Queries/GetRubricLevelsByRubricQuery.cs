﻿namespace Infrastructure.Db.Entities.RubricLevel.Queries
{
    using System.Collections.Generic;
    using Dapper;
    using Domain.Queries;
    using Domain.Queries.Criterion.RubricLevel;
    using Transactions;

    public class GetRubricLevelsByRubricQuery : IQuery<GetRubricLevelsByRubricCriterion, IEnumerable<Domain.Entities.RubricLevel>>
    {
        private readonly IDbTransactionProvider _transactionProvider;

        public GetRubricLevelsByRubricQuery(IDbTransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider;
        }

        public IEnumerable<Domain.Entities.RubricLevel> Ask(GetRubricLevelsByRubricCriterion criterion)
        {
            return _transactionProvider
                .CurrentTransaction
                .Connection
                .Query<Domain.Entities.RubricLevel>(@"
                SELECT
                    Id,
                    Description,
                    Points
                FROM
                    RubricLevel
                WHERE
                    RubricId = @rubric
                ORDER BY Points",
                    new
                    {
                        rubric = criterion.RubricId
                    });
        }
    }
}
