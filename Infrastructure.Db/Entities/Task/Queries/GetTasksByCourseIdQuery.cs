﻿namespace Infrastructure.Db.Entities.Task.Queries
{
    using System.Collections.Generic;
    using Dapper;
    using Domain.Queries;
    using Domain.Queries.Criterion.Task;
    using Infrastructure.Db.Transactions;

    public class GetTasksByCourseIdQuery : IQuery<GetTaskByCourseIdCriterion, IEnumerable<Domain.Entities.Task>>
    {
        private readonly IDbTransactionProvider _transactionProvider;

        public GetTasksByCourseIdQuery(IDbTransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider;
        }
        public IEnumerable<Domain.Entities.Task> Ask(GetTaskByCourseIdCriterion criterion)
        {
            return _transactionProvider
                .CurrentTransaction
                .Connection
                .Query<Domain.Entities.Task>(@"
                    SELECT
                        Id,
                        MoodleId,
                        Name,
                        Description,
                        AllowSubmissionFromDate,
                        DueDate,
                        MaxGrade,
                        GradingDate
                    FROM Task
                    WHERE CourseId=@courseid;",
                    new
                    {
                        courseid=criterion.CourseId
                    });
        }
    }
}
