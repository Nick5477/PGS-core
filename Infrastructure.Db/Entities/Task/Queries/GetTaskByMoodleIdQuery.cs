﻿namespace Infrastructure.Db.Entities.Task.Queries
{
    using Dapper;
    using Domain.Queries;
    using Domain.Queries.Criterion.Task;
    using Infrastructure.Db.Transactions;

    class GetTaskByMoodleIdQuery : IQuery<GetTaskByMoodleIdCriterion,Domain.Entities.Task>
    {
        private readonly IDbTransactionProvider _transactionProvider;

        public GetTaskByMoodleIdQuery(IDbTransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider;
        }

        public Domain.Entities.Task Ask(GetTaskByMoodleIdCriterion criterion)
        {
            return _transactionProvider
                .CurrentTransaction
                .Connection
                .QueryFirstOrDefault<Domain.Entities.Task>(@"
                SELECT
                    Id,
                    MoodleId,
                    Name,
                    Description,
                    AllowSubmissionFromDate,
                    DueDate,
                    MaxGrade,
                    MoodleMaxGrade,
                    GradingDate
                FROM Task
                WHERE MoodleId=@moodleid",
                new
                {
                    moodleid=criterion.TaskMoodleId
                });
        }
    }
}
