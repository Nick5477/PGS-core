﻿namespace Infrastructure.Db.Entities.Task.Queries
{
    using System;
    using System.Collections.Generic;

    using Domain.Entities;
    using Domain.Queries;
    using Domain.Queries.Criterion.Task;

    using Infrastructure.Db.Mysql;
    using Infrastructure.Db.Postgres;

    using MySql.Data.MySqlClient;

    using Npgsql;

    public class SyncTasksQuery : IQuery<SyncTasksCriterion, IEnumerable<Task>>
    {
        private static readonly DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public IEnumerable<Task> Ask(SyncTasksCriterion criterion)
        {
            //List<Task> tasks = new List<Task>();
            //using (MySqlConnection connection = MysqlConnectionProvider.CreateConnection())
            //{
            //    connection.Open();
            //    string moodleQuery =
            //        @"SELECT id, course, name, intro, duedate, allowsubmissionsfromdate, grade, gradingdate, maxworks, peergrading 
            //          FROM moodle.mdl_peergradingmodule
            //          WHERE course=@courseid";

            //    MySqlCommand command = new MySqlCommand(moodleQuery, connection);
            //    command.Parameters.AddWithValue("courseid", criterion.CourseMoodleId);

            //    var reader = command.ExecuteReader();

            //    while (reader.Read())
            //    {
            //        var pg = (PeerGradingEnum)Enum.ToObject(typeof(PeerGradingEnum), reader.GetInt32("peergrading"));
            //        tasks.Add(new Task(
            //            reader.GetInt32("id"),
            //            reader.GetString("name"),
            //            reader.GetString("intro"),
            //            epoch.AddSeconds(reader.GetInt64("allowsubmissionsfromdate")),
            //            epoch.AddSeconds(reader.GetInt64("duedate")),
            //            reader.GetInt32("grade"),
            //            epoch.AddSeconds(reader.GetInt64("gradingdate")),
            //            reader.GetInt32("maxworks"),
            //            pg));
            //    }
            //}
            //return tasks;

            List<Task> tasks = new List<Task>();
            using (var connection = PostgresConnectionProvider.CreateConnection())
            {
                connection.Open();
                string moodleQuery =
                    @"SELECT id, course, name, intro, duedate, allowsubmissionsfromdate, grade, gradingdate, maxworks, peergrading 
                      FROM mdl_peergradingmodule
                      WHERE course=@courseid";

                NpgsqlCommand command = new NpgsqlCommand(moodleQuery, connection);
                command.Parameters.AddWithValue("courseid", criterion.CourseMoodleId);

                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    var pg = (PeerGradingEnum)Enum.ToObject(typeof(PeerGradingEnum), reader.GetInt32(9));
                    tasks.Add(new Task(
                        reader.GetInt32(0),
                        reader.GetString(2),
                        reader.GetString(3),
                        epoch.AddSeconds(reader.GetInt64(5)),
                        epoch.AddSeconds(reader.GetInt64(4)),
                        reader.GetInt32(6),
                        epoch.AddSeconds(reader.GetInt64(7)),
                        reader.GetInt32(8),
                        pg));
                }
            }
            return tasks;
        }
    }
}
