﻿namespace Infrastructure.Db.Entities.Task.Commands
{
    using Domain.Commands;
    using Domain.Commands.Contexts.Task;
    using Domain.Services;

    public class AddTaskCommand : ICommand<AddTaskCommandContext>
    {
        private readonly IEntityService<Domain.Entities.Task> _service;

        public AddTaskCommand(IEntityService<Domain.Entities.Task> service)
        {
            _service = service;
        }
        public void Execute(AddTaskCommandContext commandContext)
        {
            _service.Add(commandContext.NewTask);
        }
    }
}
