﻿namespace Infrastructure.Db.Entities.OrdinalGrade.Queries
{
    using Domain.Queries;
    using Domain.Queries.Criterion.OrdinalGrade;
    using Transactions;
    using Dapper;

    public class IsGradedSubmissionsByUserQuery : IQuery<IsGradedSubmissionsByUserCriterion, bool>
    {
        private readonly IDbTransactionProvider _transactionProvider;

        public IsGradedSubmissionsByUserQuery(IDbTransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider;
        }

        public bool Ask(IsGradedSubmissionsByUserCriterion criterion)
        {
            return _transactionProvider
                       .CurrentTransaction
                       .Connection
                       .ExecuteScalar<int>(@"
                SELECT Count(*)
                FROM OrdinalGrade
                WHERE idTask=@task AND idGrader=@grader",
                           new
                           {
                               task = criterion.TaskId,
                               grader = criterion.StudentId
                           }) > 0;
        }
    }
}

