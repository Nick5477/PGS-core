﻿namespace Infrastructure.Db.Entities.OrdinalGrade.Queries
{
    using System.Collections.Generic;
    using Domain.Queries;
    using Domain.Queries.Criterion.OrdinalGrade;
    using Transactions;
    using System.Reflection;
    using Dapper;
    using System.Linq;

    public class GetOrdinalGradesByTaskQuery : IQuery<GetOrdinalGradesByTaskCriterion, IEnumerable<Domain.Entities.OrdinalGrade>>
    {
        private readonly IDbTransactionProvider _transactionProvider;

        public GetOrdinalGradesByTaskQuery(IDbTransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider;
        }

        public IEnumerable<Domain.Entities.OrdinalGrade> Ask(GetOrdinalGradesByTaskCriterion criterion)
        {
            var gradesWithGrader = _transactionProvider
                .CurrentTransaction
                .Connection
                .Query<Domain.Entities.OrdinalGrade, Domain.Entities.Task, Domain.Entities.User, Domain.Entities.OrdinalGrade>(@"
                    SELECT 
                        o.Id,
                        o.Grade,
                        o.idGrader,
                        o.idStudent1,
                        o.idStudent2,
                        t.Id,
                        t.Name,
                        t.Description,
                        t.DueDate,
                        t.MaxGrade,
                        t.MoodleMaxGrade,
                        t.AllowSubmissionFromDate,
                        t.DueDate,
                        t.GradingDate,
                        t.IsDistributed,
                        t.MaxWorks,
                        u.Id,
                        u.Nickname
                    FROM OrdinalGrade o
                    JOIN Task t ON o.idTask=t.Id
                    JOIN User u ON o.idGrader=u.Id
                    WHERE t.Id = @taskid",
                    (ordinalGrade, task, grader) =>
                    {
                        ordinalGrade.GetType().GetProperty("Task").SetValue(ordinalGrade, task);
                        ordinalGrade.GetType().GetProperty("Grader").SetValue(ordinalGrade, grader);

                        return ordinalGrade;
                    },
                    new
                    {
                        taskid = criterion.Task.Id
                    });

            var gradesWithFirstStudent = _transactionProvider
                .CurrentTransaction
                .Connection
                .Query<Domain.Entities.OrdinalGrade, Domain.Entities.Task, Domain.Entities.User, Domain.Entities.OrdinalGrade>(@"
                    SELECT 
                        o.Id,
                        o.Grade,
                        o.idGrader,
                        o.idStudent1,
                        o.idStudent2,
                        t.Id,
                        t.Name,
                        t.Description,
                        t.DueDate,
                        t.MaxGrade,
                        t.MoodleMaxGrade,
                        t.AllowSubmissionFromDate,
                        t.DueDate,
                        t.GradingDate,
                        t.IsDistributed,
                        t.MaxWorks,
                        u.Id,
                        u.Nickname
                    FROM OrdinalGrade o
                    JOIN Task t ON o.idTask=t.Id
                    JOIN User u ON o.idStudent1=u.Id
                    WHERE t.Id = @taskid",
                    (ordinalGrade, task, firstStudent) =>
                    {
                        ordinalGrade.GetType().GetProperty("Task").SetValue(ordinalGrade, task);
                        ordinalGrade.GetType().GetProperty("FirstStudent").SetValue(ordinalGrade, firstStudent);

                        return ordinalGrade;
                    },
                    new
                    {
                        taskid = criterion.Task.Id
                    });

            var gradesWithSecondStudent = 
                _transactionProvider
                    .CurrentTransaction
                    .Connection
                    .Query<Domain.Entities.OrdinalGrade, Domain.Entities.Task, Domain.Entities.User, Domain.Entities.OrdinalGrade>(@"
                    SELECT 
                        o.Id,
                        o.Grade,
                        o.idGrader,
                        o.idStudent1,
                        o.idStudent2,
                        t.Id,
                        t.Name,
                        t.Description,
                        t.DueDate,
                        t.MaxGrade,
                        t.MoodleMaxGrade,
                        t.AllowSubmissionFromDate,
                        t.DueDate,
                        t.GradingDate,
                        t.IsDistributed,
                        t.MaxWorks,
                        u.Id,
                        u.Nickname
                    FROM OrdinalGrade o
                    JOIN Task t ON o.idTask=t.Id
                    JOIN User u ON o.idStudent2=u.Id
                    WHERE t.Id = @taskid",
                        (ordinalGrade, task, secondStudent) =>
                        {
                            ordinalGrade.GetType().GetProperty("Task").SetValue(ordinalGrade, task);
                            ordinalGrade.GetType().GetProperty("SecondStudent").SetValue(ordinalGrade, secondStudent);

                            return ordinalGrade;
                        },
                        new
                        {
                            taskid = criterion.Task.Id
                        });

            foreach (var grade in gradesWithGrader)
            {
                grade.FirstStudent = gradesWithFirstStudent.FirstOrDefault(g => g.Id == grade.Id)?.FirstStudent;
                grade.SecondStudent = gradesWithSecondStudent.FirstOrDefault(g => g.Id == grade.Id)?.SecondStudent;
            }
            return gradesWithGrader;
        }
    }
}
