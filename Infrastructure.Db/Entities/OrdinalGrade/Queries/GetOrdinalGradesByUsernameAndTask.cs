﻿namespace Infrastructure.Db.Entities.OrdinalGrade.Queries
{
    using System.Collections.Generic;
    using Domain.Queries;
    using Domain.Queries.Criterion.OrdinalGrade;
    using Transactions;
    using Dapper;
    using OrdinalGrade = Domain.Entities.OrdinalGrade;
    using Task = Domain.Entities.Task;
    using System.Reflection;

    public class GetOrdinalGradesByUsernameAndTask : IQuery<GetOrdinalGradesByUsernameAndTaskCriterion, IEnumerable<Domain.Entities.OrdinalGrade>>
    {
        private readonly IDbTransactionProvider _transactionProvider;

        public GetOrdinalGradesByUsernameAndTask(IDbTransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider;
        }

        public IEnumerable<OrdinalGrade> Ask(GetOrdinalGradesByUsernameAndTaskCriterion criterion)
        {
            return _transactionProvider
                .CurrentTransaction
                .Connection
                .Query<OrdinalGrade, Task, OrdinalGrade>(@"
                    SELECT 
                        o.Id,
                        o.Grade,
                        o.idGrader,
                        o.idStudent1,
                        o.idStudent2,
                        t.Name,
                        t.Description,
                        t.DueDate,
                        t.MaxGrade,
                        t.MoodleMaxGrade,
                        t.IsDistributed,
                        t.MaxWorks
                    FROM
                        OrdinalGrade o
                    JOIN Task t ON o.idTask=t.Id
                    JOIN User ON o.idGrader=u.Id
                    WHERE t.Id = @taskid AND u.Nickname=@username",
                    (ordinalGrade, task) =>
                    {
                        ordinalGrade.GetType().GetProperty("Task").SetValue(ordinalGrade, task);

                        return ordinalGrade;
                    },
                    new
                    {
                        taskid = criterion.Task.Id,
                        username = criterion.Username
                    });
        }
    }
}
