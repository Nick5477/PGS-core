﻿namespace Infrastructure.Db.Entities.Submission.Queries
{
    using Domain.Queries;
    using Domain.Queries.Criterion.Submission;
    using System.Collections.Generic;
    using Transactions;
    using Dapper;
    using System.Linq;
    using System.Reflection;
    using Domain.Entities;

    public class GetSubmissionsByTaskQuery : IQuery<GetSubmissionsByTaskCriterion, List<Submission>>
    {
        private readonly IDbTransactionProvider _transactionProvider;

        public GetSubmissionsByTaskQuery(IDbTransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider;
        }

        public List<Submission> Ask(GetSubmissionsByTaskCriterion criterion)
        {
            return _transactionProvider
                .CurrentTransaction
                .Connection
                .Query<Submission, File, Task, User, Submission>(@"
                SELECT
                    s.Id,
                    s.TextAnswer,
                    s.SummaryGrade,
                    s.Priority,
                    s.UploadTime,
                    f.Id,
                    f.FileName,
                    f.Path,
                    t.Id,
                    t.Name,
                    t.Description,
                    u.Id,
                    u.Nickname
                FROM
                    Submission s
                    JOIN File f ON f.FileName=s.FileAnswer
                    JOIN Task t ON t.Id=s.TaskId
                    JOIN User u ON u.Id=s.UserId
                WHERE
                    s.TaskId=@task",
                    (submission, file, task, user) =>
                    {
                        submission.GetType().GetProperty("FileAnswer").SetValue(submission, file);
                        submission.GetType().GetProperty("Task").SetValue(submission, task);
                        submission.GetType().GetProperty("User").SetValue(submission, user);

                        return submission;
                    },
                    new
                    {
                        task = criterion.Task.Id
                    })
                .ToList();
        }
    }
}
