﻿namespace Infrastructure.Db.Entities.Submission.Queries
{
    using System.Linq;
    using System.Reflection;
    using Dapper;
    using Domain.Entities;
    using Domain.Queries;
    using Domain.Queries.Criterion.Submission;
    using Infrastructure.Db.Transactions;

    public class GetUserSubmissionQuery : IQuery<GetUserSubmissionCriterion,Domain.Entities.Submission>
    {
        private readonly IDbTransactionProvider _transactionProvider;

        public GetUserSubmissionQuery(IDbTransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider;
        }

        public Domain.Entities.Submission Ask(GetUserSubmissionCriterion criterion)
        {
            return _transactionProvider
                .CurrentTransaction
                .Connection
                .Query<Domain.Entities.Submission, File, Domain.Entities.Submission>(@"
                 SELECT
                    s.Id,
                    s.TextAnswer,
                    s.SummaryGrade,
                    s.Priority,
                    s.UploadTime,
                    f.Id,
                    f.FileName,
                    f.Path
                FROM Submission s
                JOIN File f ON f.FileName=s.FileAnswer
                JOIN User u ON u.Id=s.UserId
                WHERE u.Nickname=@username AND s.TaskId=@taskid",
                    (submission, file) =>
                    {
                        submission.GetType().GetProperty("FileAnswer").SetValue(submission, file);

                        return submission;
                    },
                    new
                    {
                        username = criterion.Username,
                        taskid = criterion.TaskId
                    })
                .FirstOrDefault();
        }
    }
}
