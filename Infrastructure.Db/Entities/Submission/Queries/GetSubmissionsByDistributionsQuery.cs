﻿namespace Infrastructure.Db.Entities.Submission.Queries
{
    using System;

    using Domain.Queries;
    using System.Collections.Generic;
    using Domain.Queries.Criterion.Submission;
    using Transactions;
    using System.Linq;
    using Dapper;
    using Domain.Entities;
    using System.Reflection;

    public class GetSubmissionsByDistributionsQuery : IQuery<GetSubmissionsByDistributionsCriterion, List<Submission>>
    {
        private readonly IDbTransactionProvider _transactionProvider;

        public GetSubmissionsByDistributionsQuery(IDbTransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider;
        }

        public List<Submission> Ask(GetSubmissionsByDistributionsCriterion criterion)
        {
            List<Submission> submissions = null;
            try
            {
                submissions = _transactionProvider
                    .CurrentTransaction
                    .Connection
                    .Query<Submission, File, Task, User, Submission>(@"
                SELECT
                    s.Id,
                    s.TextAnswer,
                    s.SummaryGrade,
                    s.Priority,
                    s.TaskId,
                    s.UserId,
                    s.UploadTime,
                    f.Id,
                    f.FileName,
                    f.Path,
                    t.Id,
                    t.Name,
                    t.Description,
                    u.Id,
                    u.Nickname
                FROM
                    Submission s
                    JOIN File f ON f.FileName=s.FileAnswer
                    JOIN Task t ON t.Id=s.TaskId
                    JOIN User u ON u.Id=s.UserId
                WHERE
                    s.TaskId=@task AND u.Id IN @ids",
                        (submission, file, task, user) =>
                        {
                            submission.GetType().GetProperty("FileAnswer").SetValue(submission, file);
                            submission.GetType().GetProperty("Task").SetValue(submission, task);
                            submission.GetType().GetProperty("User").SetValue(submission, user);

                            return submission;
                        },
                        new
                        {
                            task = criterion.Distributions.First().TaskId,
                            ids = criterion.Distributions.Select(x => x.StudentId).ToArray()
                        })
                    .ToList();
            }
            catch (Exception e)
            {
                return submissions;
            }
            return submissions;
        }
    }
}
