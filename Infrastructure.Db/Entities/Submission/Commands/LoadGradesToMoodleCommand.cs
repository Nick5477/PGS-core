﻿namespace Infrastructure.Db.Entities.Submission.Commands
{
    using System;
    using System.Collections.Generic;

    using Domain.Commands;
    using Domain.Commands.Contexts.Submission;
    using Domain.Entities;
    using Domain.Services.Submission;
    using Domain.UnitOfWork;

    using Infrastructure.Db.Mysql;
    using Infrastructure.Db.Postgres;

    using MySql.Data.MySqlClient;

    using Npgsql;

    public class LoadGradesToMoodleCommand : ICommand<LoadGradesToMoodleCommandContext>
    {
        private static readonly DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        private readonly ISubmissionService _submissionService;

        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public LoadGradesToMoodleCommand(
            ISubmissionService submissionService,
            IUnitOfWorkFactory unitOfWorkFactory)
        {
            _submissionService = submissionService;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public void Execute(LoadGradesToMoodleCommandContext commandContext)
        {
            //List<Submission> submissions;
            //using (var unitOfWork = _unitOfWorkFactory.Create())
            //{
            //    submissions = _submissionService.GetSubmissionsByTask(commandContext.Task);

            //    unitOfWork.Commit();
            //}


            //using (var connection = MysqlConnectionProvider.CreateConnection())
            //{
            //    connection.Open();
            //    string getItemIdQueryText =
            //        @"SELECT id
            //          FROM moodle.mdl_grade_items
            //          WHERE courseid=@course AND itemname=@item AND itemmodule='peergradingmodule'";
            //    MySqlCommand getItemIdQuery = new MySqlCommand(getItemIdQueryText, connection);
            //    getItemIdQuery.Parameters.AddWithValue("course", commandContext.Task.Course.MoodleId);
            //    getItemIdQuery.Parameters.AddWithValue("item", commandContext.Task.Name);
            //    var itemId = (long)getItemIdQuery.ExecuteScalar();

            //    string getUserIdQueryText =
            //        @"SELECT id
            //          FROM moodle.mdl_user
            //          WHERE username=@username";
            //    MySqlCommand getUserIdQuery = new MySqlCommand(getUserIdQueryText, connection);

            //    string deleteGradeCommandText =
            //        @"DELETE FROM moodle.mdl_grade_grades
            //          WHERE itemid=@item AND userid=@user";

            //    MySqlCommand deleteGradeCommand =
            //        new MySqlCommand(deleteGradeCommandText, connection);

            //    string addGradeCommandText =
            //        @"INSERT INTO moodle.mdl_grade_grades
            //          (
            //            itemid,
            //            userid,
            //            rawgrade,
            //            rawgrademax,
            //            rawgrademin,
            //            finalgrade,
            //            hidden,
            //            locked,
            //            locktime,
            //            exported,
            //            overridden,
            //            excluded,
            //            feedbackformat,
            //            informationformat,
            //            timemodified,
            //            aggregationstatus,
            //            aggregationweight
            //          )
            //        VALUES
            //        (
            //            @item,
            //            @user,
            //            @rawgrade,
            //            @grademax,
            //            0,
            //            @rawgrade,
            //            0,
            //            0,
            //            0,
            //            0,
            //            0,
            //            0,
            //            0,
            //            0,
            //            @time,
            //            'used',
            //            1.0
            //        )";
            //    MySqlCommand addGradeCommand = new MySqlCommand(addGradeCommandText, connection);

            //    foreach (var submission in submissions)
            //    {
            //        getUserIdQuery.Parameters.Clear();
            //        getUserIdQuery.Parameters.AddWithValue("username", submission.User.Nickname);
            //        var userId = (long)getUserIdQuery.ExecuteScalar();

            //        deleteGradeCommand.Parameters.Clear();
            //        deleteGradeCommand.Parameters.AddWithValue("user", userId);
            //        deleteGradeCommand.Parameters.AddWithValue("item", itemId);
            //        deleteGradeCommand.ExecuteNonQuery();

            //        addGradeCommand.Parameters.Clear();
            //        addGradeCommand.Parameters.AddWithValue("item", itemId);
            //        addGradeCommand.Parameters.AddWithValue("user", userId);
            //        addGradeCommand.Parameters.AddWithValue(
            //            "rawgrade", 
            //            (double)submission.SummaryGrade);
            //        addGradeCommand.Parameters.AddWithValue("grademax", (double)commandContext.Task.MoodleMaxGrade);
            //        DateTimeOffset dateTimeOffset = new DateTimeOffset(DateTime.UtcNow);
            //        addGradeCommand.Parameters.AddWithValue("time", dateTimeOffset.ToUnixTimeSeconds());

            //        addGradeCommand.ExecuteNonQuery();
            //    }
            //}

            List<Submission> submissions;
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                submissions = _submissionService.GetSubmissionsByTask(commandContext.Task);

                unitOfWork.Commit();
            }


            using (var connection = PostgresConnectionProvider.CreateConnection())
            {
                connection.Open();
                string getItemIdQueryText =
                    @"SELECT id
                      FROM mdl_grade_items
                      WHERE courseid=@course AND itemname=@item AND itemmodule='peergradingmodule'";
                NpgsqlCommand getItemIdQuery = new NpgsqlCommand(getItemIdQueryText, connection);
                getItemIdQuery.Parameters.AddWithValue("course", commandContext.Task.Course.MoodleId);
                getItemIdQuery.Parameters.AddWithValue("item", commandContext.Task.Name);
                var itemId = (long)getItemIdQuery.ExecuteScalar();

                string getUserIdQueryText =
                    @"SELECT id
                      FROM mdl_user
                      WHERE username=@username OR email=@username";
                NpgsqlCommand getUserIdQuery = new NpgsqlCommand(getUserIdQueryText, connection);

                string deleteGradeCommandText =
                    @"DELETE FROM mdl_grade_grades
                      WHERE itemid=@item AND userid=@user";

                NpgsqlCommand deleteGradeCommand =
                    new NpgsqlCommand(deleteGradeCommandText, connection);

                string addGradeCommandText =
                    @"INSERT INTO mdl_grade_grades
                      (
                        itemid,
                        userid,
                        rawgrade,
                        rawgrademax,
                        rawgrademin,
                        finalgrade,
                        hidden,
                        locked,
                        locktime,
                        exported,
                        overridden,
                        excluded,
                        feedbackformat,
                        informationformat,
                        timemodified,
                        aggregationstatus,
                        aggregationweight
                      )
                    VALUES
                    (
                        @item,
                        @user,
                        @rawgrade,
                        @grademax,
                        0,
                        @rawgrade,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        @time,
                        'used',
                        1.0
                    )";
                NpgsqlCommand addGradeCommand = new NpgsqlCommand(addGradeCommandText, connection);

                foreach (var submission in submissions)
                {
                    getUserIdQuery.Parameters.Clear();
                    getUserIdQuery.Parameters.AddWithValue("username", submission.User.Nickname);
                    var userId = (long)getUserIdQuery.ExecuteScalar();

                    deleteGradeCommand.Parameters.Clear();
                    deleteGradeCommand.Parameters.AddWithValue("user", userId);
                    deleteGradeCommand.Parameters.AddWithValue("item", itemId);
                    deleteGradeCommand.ExecuteNonQuery();

                    addGradeCommand.Parameters.Clear();
                    addGradeCommand.Parameters.AddWithValue("item", itemId);
                    addGradeCommand.Parameters.AddWithValue("user", userId);
                    addGradeCommand.Parameters.AddWithValue(
                        "rawgrade",
                        (double)submission.SummaryGrade);
                    addGradeCommand.Parameters.AddWithValue("grademax", (double)commandContext.Task.MoodleMaxGrade);
                    DateTimeOffset dateTimeOffset = new DateTimeOffset(DateTime.UtcNow);
                    addGradeCommand.Parameters.AddWithValue("time", dateTimeOffset.ToUnixTimeSeconds());

                    addGradeCommand.ExecuteNonQuery();
                }
            }
        }
    }
}
