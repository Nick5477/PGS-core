﻿namespace Infrastructure.Db.Entities.CardinalGrade.Queries
{
    using System.Collections.Generic;
    using Domain.Queries;
    using Domain.Queries.Criterion.CardinalGrade;
    using Transactions;
    using Dapper;
    using System.Reflection;

    using Domain.Services.User;

    public class GetCardinalGradesByTaskQuery : IQuery<GetCardinalGradesByTaskCriterion, IEnumerable<Domain.Entities.CardinalGrade>>
    {
        private readonly IDbTransactionProvider _transactionProvider;

        private readonly IQueryBuilder _queryBuilder;

        private readonly IUserService _userService;

        public GetCardinalGradesByTaskQuery(
            IDbTransactionProvider transactionProvider,
            IQueryBuilder queryBuilder,
            IUserService userService)
        {
            _transactionProvider = transactionProvider;
            _queryBuilder = queryBuilder;
            _userService = userService;
        }

        public IEnumerable<Domain.Entities.CardinalGrade> Ask(GetCardinalGradesByTaskCriterion criterion)
        {
            var cardinalGrades = _transactionProvider
                .CurrentTransaction
                .Connection
                .Query<Domain.Entities.CardinalGrade, Domain.Entities.User, Domain.Entities.Submission, Domain.Entities.CardinalGrade>(@"
                    SELECT
                        g.Id,
                        g.Grade,
                        u.Id,
                        u.Nickname,
                        s.Id,
                        s.TextAnswer,
                        s.Priority,
                        s.SummaryGrade
                    FROM 
                        CardinalGrade g
                        JOIN Submission s ON s.Id=g.SubmissionId
                        JOIN User u ON u.Id=g.GraderId
                    WHERE s.TaskId=@task",
                    (grade, grader, submission) =>
                    {
                        grade.GetType().GetProperty("Grader").SetValue(grade, grader);
                        grade.GetType().GetProperty("Submission").SetValue(grade, submission);

                        return grade;
                    },
                    new
                    {
                        task = criterion.Task.Id
                    });

            foreach (var grade in cardinalGrades)
            {
                grade.Answers =
                    _queryBuilder
                        .For<Dictionary<Domain.Entities.Rubric, Domain.Entities.RubricLevel>>()
                        .With(
                            new GetCardinalAnswersCriterion()
                            {
                                CardinalGrade = grade
                            });
                grade.Submission.User = _userService.GetUserBySubmission(grade.Submission);
            }

            return cardinalGrades;
        }
    }
}
