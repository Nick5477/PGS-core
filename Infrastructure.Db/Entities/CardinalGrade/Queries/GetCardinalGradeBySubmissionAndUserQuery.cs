﻿namespace Infrastructure.Db.Entities.CardinalGrade.Queries
{
    using Domain.Queries;
    using Domain.Queries.Criterion.CardinalGrade;
    using System.Collections.Generic;
    using System.Linq;

    public class GetCardinalGradeBySubmissionAndUserQuery : IQuery<GetCardinalGradeBySubmissionAndUserCriterion, Domain.Entities.CardinalGrade>
    {
        private readonly IQueryBuilder _queryBuilder;

        public GetCardinalGradeBySubmissionAndUserQuery(
            IQueryBuilder queryBuilder)
        {
            _queryBuilder = queryBuilder;
        }

        public Domain.Entities.CardinalGrade Ask(GetCardinalGradeBySubmissionAndUserCriterion criterion)
        {
            return
                _queryBuilder
                    .For<IEnumerable<Domain.Entities.CardinalGrade>>()
                    .With(
                        new GetCardinalGradesByTaskAndUserCriterion()
                        {
                            Task = criterion.Submission.Task,
                            User = criterion.User
                        })
                    .FirstOrDefault(
                        cg => cg.Submission.Id == criterion.Submission.Id);
        }
    }
}
