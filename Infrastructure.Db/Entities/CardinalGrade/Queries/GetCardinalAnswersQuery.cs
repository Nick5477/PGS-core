﻿namespace Infrastructure.Db.Entities.CardinalGrade.Queries
{
    using System.Collections.Generic;
    using Dapper;
    using Domain.Queries;
    using Domain.Queries.Criterion.CardinalGrade;
    using Domain.Services;
    using Transactions;
    using System.Linq;

    public class GetCardinalAnswersQuery : IQuery<GetCardinalAnswersCriterion, Dictionary<Domain.Entities.Rubric, Domain.Entities.RubricLevel>>
    {
        private readonly IDbTransactionProvider _transactionProvider;
        private readonly IEntityService<Domain.Entities.Rubric> _rubricEntityService;

        public GetCardinalAnswersQuery(
            IDbTransactionProvider transactionProvider,
            IEntityService<Domain.Entities.Rubric> rubricEntityService)
        {
            _transactionProvider = transactionProvider;
            _rubricEntityService = rubricEntityService;
        }

        public Dictionary<Domain.Entities.Rubric, Domain.Entities.RubricLevel> Ask(GetCardinalAnswersCriterion criterion)
        {
            var rubrics =
                _transactionProvider
                    .CurrentTransaction
                    .Connection
                    .Query<Domain.Entities.Rubric>(@"
                        SELECT
                            r.Id                           
                        FROM
                            Rubric r
                        JOIN CardinalGradeRubric cgr ON cgr.RubricId=r.Id
                        WHERE cgr.CardinalGradeId = @id",
                        new
                        {
                            id = criterion.CardinalGrade.Id
                        });
            var rubricLevels = 
                _transactionProvider
                .CurrentTransaction
                .Connection
                    .Query<Domain.Entities.Rubric>(@"
                        SELECT
                            rl.Id                           
                        FROM
                            RubricLevel rl
                        JOIN CardinalGradeRubric cgr ON cgr.RubricLevelId=rl.Id
                        WHERE cgr.CardinalGradeId = @id",
                        new
                        {
                            id = criterion.CardinalGrade.Id
                        });
            Dictionary<Domain.Entities.Rubric, Domain.Entities.RubricLevel> resultDictionary = 
                new Dictionary<Domain.Entities.Rubric, Domain.Entities.RubricLevel>();
            foreach (var rubric in rubrics)
            {
                var fullRubric = _rubricEntityService.Get(rubric.Id);
                resultDictionary.Add(
                    fullRubric, 
                    fullRubric.Levels.FirstOrDefault(rl => rubricLevels.Any(x => x.Id == rl.Id)));
            }

            return resultDictionary;
        }
    }
}
