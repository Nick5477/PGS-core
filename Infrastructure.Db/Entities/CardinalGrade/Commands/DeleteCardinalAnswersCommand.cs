﻿namespace Infrastructure.Db.Entities.CardinalGrade.Commands
{
    using Dapper;

    using Domain.Commands;
    using Domain.Commands.Contexts.CardinalGrade;

    using Infrastructure.Db.Transactions;

    public class DeleteCardinalAnswersCommand : ICommand<DeleteCardinalAnswersCommandContext>
    {
        private readonly IDbTransactionProvider _transactionProvider;

        public DeleteCardinalAnswersCommand(IDbTransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider;
        }

        public void Execute(DeleteCardinalAnswersCommandContext commandContext)
        {
            _transactionProvider
                .CurrentTransaction
                .Connection
                .Execute(@"
                    DELETE FROM CardinalGradeRubric
                    WHERE CardinalGradeId=@id",
                    new
                    {
                        id = commandContext.CardinalGrade.Id
                    });
        }
    }
}
