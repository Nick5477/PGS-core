﻿namespace Infrastructure.Db.Entities.User.Queries
{
    using System.Linq;

    using Dapper;

    using Domain.Entities;
    using Domain.Queries;
    using Domain.Queries.Criterion.User;

    using Infrastructure.Db.Transactions;

    public class GetUserBySubmissionQuery : IQuery<GetUserBySubmissionCriterion, User>
    {
        private readonly IDbTransactionProvider _transactionProvider;

        public GetUserBySubmissionQuery(
            IDbTransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider;
        }

        public User Ask(GetUserBySubmissionCriterion criterion)
        {
            return
                _transactionProvider
                    .CurrentTransaction
                    .Connection
                    .Query<User>(
                        @"
                        SELECT
                            u.Id,
                            u.Nickname
                        FROM User u
                        JOIN Submission s ON s.UserId=u.Id
                        WHERE s.Id = @id",
                        new
                        {
                            id = criterion.Submission.Id
                        })
                    .FirstOrDefault();
        }
    }
}
