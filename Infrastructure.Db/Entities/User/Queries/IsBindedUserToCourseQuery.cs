﻿namespace Infrastructure.Db.Entities.User.Queries
{
    using Dapper;
    using Domain.Queries;
    using Domain.Queries.Criterion.User;
    using Transactions;

    public class IsBindedUserToCourseQuery : IQuery<IsBindedUserToCourseCriterion, bool>
    {
        private readonly IDbTransactionProvider _transactionProvider;

        public IsBindedUserToCourseQuery(IDbTransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider;
        }
        public bool Ask(IsBindedUserToCourseCriterion criterion)
        {
            return _transactionProvider
                .CurrentTransaction
                .Connection
                .QueryFirst<int>(@"
                SELECT COUNT(*)
                FROM UserCourse
                WHERE UserId=@userid AND CourseId=@courseid",
                new
                {
                    userid=criterion.UserId,
                    courseid=criterion.CourseId
                }) > 0;
        }
    }
}
