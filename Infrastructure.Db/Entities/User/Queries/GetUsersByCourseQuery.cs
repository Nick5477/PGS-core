﻿namespace Infrastructure.Db.Entities.User.Queries
{
    using System.Collections.Generic;
    using System.Reflection;
    using Dapper;
    using Domain.Entities;
    using Domain.Queries;
    using Domain.Queries.Criterion.User;
    using Transactions;

    public class GetUsersByCourseQuery : IQuery<GetUsersByCourseCriterion, IEnumerable<Domain.Entities.User>>
    {
        private readonly IDbTransactionProvider _transactionProvider;

        public GetUsersByCourseQuery(IDbTransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider;
        }

        public IEnumerable<Domain.Entities.User> Ask(GetUsersByCourseCriterion criterion)
        {
            return _transactionProvider
                .CurrentTransaction
                .Connection
                .Query<Domain.Entities.User, Role, Domain.Entities.User>(@"
                SELECT
                    u.Id,
                    u.Nickname,
                    r.Id,
                    r.Name
                FROM User u 
                JOIN Role r ON u.RoleId=r.Id
                JOIN UserCourse uc ON uc.UserId=u.Id
                WHERE uc.CourseId = @course",
                    (user, role) =>
                    {
                        user.GetType().GetProperty("Role").SetValue(user, role);

                        return user;
                    },
                    new
                    {
                        course = criterion.CourseId
                    });
        }
    }
}
