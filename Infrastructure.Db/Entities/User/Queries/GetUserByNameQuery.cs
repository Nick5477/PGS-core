﻿namespace Infrastructure.Db.Entities.User.Queries
{
    using Domain.Queries;
    using Domain.Queries.Criterion.User;
    using Domain.Services;

    class GetUserByNameQuery : IQuery<GetUserByNameCriterion, Domain.Entities.User>
    {
        private readonly IEntityService<Domain.Entities.User> _service;

        public GetUserByNameQuery(IEntityService<Domain.Entities.User> service)
        {
            _service = service;
        }
        public Domain.Entities.User Ask(GetUserByNameCriterion criterion)
        {
            return _service.Get(criterion.Username);
        }
    }
}
