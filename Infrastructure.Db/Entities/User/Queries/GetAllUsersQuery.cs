﻿namespace Infrastructure.Db.Entities.User.Queries
{
    using System.Collections.Generic;
    using Domain.Queries;
    using Domain.Queries.Criterion.User;
    using Domain.Repositories;

    public class GetAllUsersQuery : IQuery<GetAllUsersCriterion, IEnumerable<Domain.Entities.User>>
    {
        private readonly IRepository<Domain.Entities.User> _repository;

        public GetAllUsersQuery(IRepository<Domain.Entities.User> repository)
        {
            _repository = repository;
        }
        public IEnumerable<Domain.Entities.User> Ask(GetAllUsersCriterion criterion)
        {
            return _repository.All();
        }
    }
}
