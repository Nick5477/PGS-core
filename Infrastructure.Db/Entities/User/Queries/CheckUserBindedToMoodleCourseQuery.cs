﻿namespace Infrastructure.Db.Entities.User.Queries
{
    using Domain.Queries;
    using Domain.Queries.Criterion.User;

    using Infrastructure.Db.Postgres;

    using Mysql;
    using MySql.Data.MySqlClient;

    using Npgsql;

    public class CheckUserBindedToMoodleCourseQuery : IQuery<CheckUserBindedToMoodleCourseCriterion, bool>
    {
        public bool Ask(CheckUserBindedToMoodleCourseCriterion criterion)
        {
            //long result;
            //using (MySqlConnection connection = MysqlConnectionProvider.CreateConnection())
            //{
            //    connection.Open();
            //    string moodleQuery =
            //        @"SELECT COUNT(*)
            //          FROM moodle.mdl_course c
            //          JOIN moodle.mdl_enrol e on e.courseid=c.id
            //          JOIN moodle.mdl_user_enrolments ue on ue.enrolid=e.id
            //          JOIN moodle.mdl_user u on u.id=ue.userid
            //          WHERE u.username=@username AND c.id=@courseid";
            //    MySqlCommand command = new MySqlCommand(moodleQuery, connection);
            //    command.Parameters.AddWithValue("username", criterion.Username);
            //    command.Parameters.AddWithValue("courseid", criterion.MoodleId);

            //    result = (long)command.ExecuteScalar();
            //}
            //return result > 0;

            long result;
            using (var connection = PostgresConnectionProvider.CreateConnection())
            {
                connection.Open();
                string moodleQuery =
                    @"SELECT COUNT(*)
                      FROM mdl_course c
                      JOIN mdl_enrol e on e.courseid=c.id
                      JOIN mdl_user_enrolments ue on ue.enrolid=e.id
                      JOIN mdl_user u on u.id=ue.userid
                      WHERE (u.username=@username OR u.email=@username) AND c.id=@courseid";
                NpgsqlCommand command = new NpgsqlCommand(moodleQuery, connection);
                command.Parameters.AddWithValue("username", criterion.Username);
                command.Parameters.AddWithValue("courseid", criterion.MoodleId);

                result = (long)command.ExecuteScalar();
            }
            return result > 0;
        }
    }
}
