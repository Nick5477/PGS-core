﻿namespace Infrastructure.Db.Repositories
{
    using System.Collections.Generic;
    using System.Linq;

    using Domain.Entities;
    using Domain.Repositories;
    using Dapper;

    using Domain.Services.Rubric;

    using Transactions;

    public class RubricRepository : IRepository<Rubric>
    {
        private readonly IDbTransactionProvider _transactionProvider;

        public RubricRepository(
            IDbTransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider;
        }

        public void Add(Rubric entity)
        {
            _transactionProvider
                .CurrentTransaction
                .Connection
                .Execute(@"
                    INSERT INTO Rubric
                    VALUES
                    (
                        @id,
                        @name,
                        @description,
                        @taskid
                    )",
                    new
                    {
                        id = entity.Id,
                        name = entity.Name,
                        description = entity.Description,
                        taskid = entity.Task.Id
                    });
            foreach (var level in entity.Levels)
            {
                _transactionProvider
                    .CurrentTransaction
                    .Connection
                    .Execute(@"
                        INSERT INTO RubricLevel
                        VALUES
                        (
                            @id,
                            @description,
                            @points,
                            @rubric
                        )",
                        new
                        {
                            id = level.Id,
                            description = level.Description,
                            points = level.Points,
                            rubric = entity.Id
                        });
            }
        }

        public Rubric Get(string id)
        {
            var rubric = _transactionProvider
                .CurrentTransaction
                .Connection
                .Query<Rubric>(@"
                    SELECT
                        Id,
                        Name,
                        Description
                    FROM
                        Rubric r
                    WHERE 
                        Id = @rubricid",
                    new
                    {
                        rubricid = id
                    })
                    .FirstOrDefault();

            rubric.Levels = _transactionProvider
                .CurrentTransaction
                .Connection
                .Query<RubricLevel>(@"
                SELECT
                    Id,
                    Description,
                    Points
                FROM
                    RubricLevel
                WHERE
                    RubricId = @rubric
                ORDER BY Points",
                    new
                    {
                        rubric = id
                    })
                    .ToList();

            return rubric;
        }

        public void Update(Rubric entity)
        {
            _transactionProvider
                .CurrentTransaction
                .Connection
                .Execute(@"
                    UPDATE Rubric
                    SET Name = @name,
                        Description = @description
                    WHERE Id = @id",
                    new
                    {
                        id = entity.Id,
                        name = entity.Name,
                        description = entity.Description
                    });

            foreach (var level in entity.Levels)
            {
                _transactionProvider
                    .CurrentTransaction
                    .Connection
                    .Execute(@"
                        UPDATE RubricLevel
                        SET Description = @description,
                            Points = @points
                        WHERE Id = @id",
                        new
                        {
                            id = level.Id,
                            description = level.Description,
                            points = level.Points
                        });
            }
        }

        public IEnumerable<Rubric> All()
        {
            throw new System.NotImplementedException();
        }

        public void Delete(Rubric entity)
        {
            _transactionProvider
                .CurrentTransaction
                .Connection
                .Execute(@"
                    DELETE FROM RubricLevel
                    WHERE RubricId = @id",
                    new
                    {
                        id = entity.Id
                    });
            _transactionProvider
                .CurrentTransaction
                .Connection
                .Execute(@"
                    DELETE FROM Rubric
                    WHERE Id = @id",
                    new
                    {
                        id = entity.Id
                    });
        }
    }
}
