﻿namespace Infrastructure.Db.Repositories
{
    using System.Collections.Generic;
    using Dapper;
    using Domain.Entities;
    using Domain.Repositories;
    using Transactions;

    public class OrdinalGradeRepository : IRepository<OrdinalGrade>
    {
        private readonly IDbTransactionProvider _transactionProvider;

        public OrdinalGradeRepository(IDbTransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider;
        }

        public void Add(OrdinalGrade entity)
        {
            _transactionProvider
                .CurrentTransaction
                .Connection
                .Execute(@"
                INSERT INTO OrdinalGrade
                (
                    Id,
                    idTask,
                    idGrader,
                    idStudent1,
                    idStudent2,
                    Grade
                )
                VALUES
                (
                    @id,
                    @idTask,
                    @idGrader,
                    @idStudent1,
                    @idStudent2,
                    @grade
                )",
                new
                {
                    id = entity.Id,
                    idTask = entity.Task.Id,
                    idGrader = entity.Grader.Id,
                    idStudent1 = entity.FirstStudent.Id,
                    idStudent2 = entity.SecondStudent.Id,
                    grade = entity.Grade
                });
        }

        public OrdinalGrade Get(string id)
        {
            throw new System.NotImplementedException();
        }

        public void Update(OrdinalGrade entity)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<OrdinalGrade> All()
        {
            throw new System.NotImplementedException();
        }

        public void Delete(OrdinalGrade entity)
        {
            throw new System.NotImplementedException();
        }
    }
}
