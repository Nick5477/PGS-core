﻿namespace Infrastructure.Db.Repositories
{
    using Domain.Entities;
    using Domain.Repositories;
    using System.Collections.Generic;
    using System.Linq;

    using Transactions;
    using Dapper;

    using Domain.Services.CardinalGrade;

    public class CardinalGradeRepository : IRepository<CardinalGrade>
    {
        private readonly IDbTransactionProvider _transactionProvider;

        public CardinalGradeRepository(
            IDbTransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider;
        }

        public void Add(CardinalGrade entity)
        {
            _transactionProvider
                .CurrentTransaction
                .Connection
                .Execute(@"
                    INSERT INTO CardinalGrade
                    (
                        Id,
                        GraderId,
                        SubmissionId,
                        Grade
                    )
                    VALUES
                    (
                        @id,
                        @grader,
                        @submission,
                        @grade
                    )",
                    new
                    {
                        id =entity.Id,
                        grader = entity.Grader.Id,
                        submission = entity.Submission.Id,
                        grade = entity.Grade
                    });

            foreach (var answer in entity.Answers)
            {
                _transactionProvider
                    .CurrentTransaction
                    .Connection
                    .Execute(@"
                        INSERT INTO CardinalGradeRubric
                        (
                            CardinalGradeId,
                            RubricId,
                            RubricLevelId
                        )
                        VALUES
                        (
                            @cgid,
                            @rubric,
                            @rubriclevel
                        )",
                        new
                        {
                            cgid = entity.Id,
                            rubric = answer.Key.Id,
                            rubriclevel = answer.Value.Id
                        });
            }
        }

        public CardinalGrade Get(string id)
        {
            return
                _transactionProvider
                    .CurrentTransaction
                    .Connection
                    .Query<CardinalGrade>(@"
                        SELECT
                            Id,
                            GraderId,
                            SubmissionId,
                            Grade
                        FROM
                            CardinalGrade
                        WHERE
                            Id=@idGrade",
                        new
                        {
                            idGrade = id
                        })
                .FirstOrDefault();
        }

        public void Update(CardinalGrade entity)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<CardinalGrade> All()
        {
            throw new System.NotImplementedException();
        }

        public void Delete(CardinalGrade entity)
        {
            _transactionProvider
                .CurrentTransaction
                .Connection
                .Execute(@"
                    DELETE FROM CardinalGradeRubric
                    WHERE CardinalGradeId=@id",
                    new
                    {
                        id = entity.Id
                    });
            _transactionProvider
                .CurrentTransaction
                .Connection
                .Execute(@"
                    DELETE FROM
                        CardinalGrade
                    WHERE Id=@id",
                    new
                    {
                        id = entity.Id
                    });
        }
    }
}
