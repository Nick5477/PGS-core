﻿namespace Infrastructure.Db.Repositories
{
    using System.Collections.Generic;
    using System.Linq;
    using Dapper;
    using Domain.Entities;
    using Domain.Repositories;
    using Transactions;

    public class FileRepository : IRepository<File>
    {
        private readonly IDbTransactionProvider _transactionProvider;

        public FileRepository(IDbTransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider;
        }
        public void Add(File entity)
        {
            _transactionProvider
                .CurrentTransaction
                .Connection
                .Execute(@"
                    INSERT INTO File
                    (
                        Id,
                        FileName,
                        Path
                    )
                    VALUES
                    (
                        @id,
                        @name,
                        @path
                    )",
                    new
                    {
                        id = entity.Id,
                        name = entity.FileName,
                        path = entity.Path
                    });
        }

        public File Get(string id)
        {
            return _transactionProvider
                .CurrentTransaction
                .Connection
                .Query<File>(@"
                    SELECT FileName, Path
                    FROM File
                    WHERE FileName = @name",
                    new
                    {
                        name = id 
                    })
                .FirstOrDefault();
        }

        public void Update(File entity)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<File> All()
        {
            throw new System.NotImplementedException();
        }

        public void Delete(File entity)
        {
            throw new System.NotImplementedException();
        }
    }
}
