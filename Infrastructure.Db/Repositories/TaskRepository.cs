﻿namespace Infrastructure.Db.Repositories
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Dapper;
    using Domain.Entities;
    using Domain.Repositories;
    using Transactions;

    class TaskRepository : IRepository<Task>
    {
        private readonly IDbTransactionProvider _transactionProvider;

        public TaskRepository(IDbTransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider;
        }
        public void Add(Task entity)
        {
            entity.InitializeId();
            _transactionProvider
                .CurrentTransaction
                .Connection
                .Execute(@"
                INSERT INTO Task
                (
                    Id,
                    MoodleId,
                    Name,
                    Description,
                    AllowSubmissionFromDate,
                    GradingDate,
                    DueDate,
                    MaxGrade,
                    MoodleMaxGrade,
                    CourseId,
                    IsDistributed,
                    MaxWorks,
                    PeerGrading
                )
                VALUES
                (
                    @id,
                    @moodleid,
                    @name,
                    @description,
                    @allowsubmissiondate,
                    @gradingdate,
                    @duedate,
                    @maxgrade,
                    @mmaxgrade,
                    @courseid,
                    @distr,
                    @works,
                    @peerg
                );",
                    new
                    {
                        id = entity.Id,
                        moodleid = entity.MoodleId,
                        name = entity.Name,
                        description = entity.Description,
                        allowsubmissiondate = entity.AllowSubmissionFromDate,
                        duedate = entity.DueDate,
                        maxgrade = entity.MaxGrade,
                        mmaxgrade = entity.MoodleMaxGrade,
                        courseid = entity.Course.Id,
                        distr = entity.IsDistributed,
                        works = entity.MaxWorks,
                        gradingdate = entity.GradingDate,
                        peerg = (int)entity.PeerGrading
                    });
        }

        public Task Get(string id)
        {
            return _transactionProvider
                .CurrentTransaction
                .Connection
                .Query<Task, Course, Task>(@"
                SELECT
                    t.Id,
                    t.MoodleId,
                    t.Name,
                    t.Description,
                    t.AllowSubmissionFromDate,
                    t.GradingDate,
                    t.DueDate,
                    t.MaxGrade,
                    t.MoodleMaxGrade,
                    t.IsDistributed,
                    t.MaxWorks,
                    t.PeerGrading,
                    t.CardinalGradingDescription,
                    c.Id,
                    c.MoodleId,
                    c.FullName,
                    c.ShortName
                FROM Task t
                JOIN Course c on t.CourseId=c.Id
                WHERE t.Id=@taskid",
                    (task, course) =>
                    {
                        task.GetType().GetProperty("Course").SetValue(task, course);

                        return task;
                    },
                    new
                    {
                        taskid = id
                    })
                .FirstOrDefault();
        }

        public void Update(Task entity)
        {
            _transactionProvider
                .CurrentTransaction
                .Connection
                .Execute(@"
                    UPDATE Task
                    SET IsDistributed = @isDistributed, 
                        Name = @name,
                        Description = @description,
                        AllowSubmissionFromDate = @allowSub,
                        DueDate = @duedate,
                        MaxGrade = @maxgrade,
                        MoodleMaxGrade = @mmaxgrade,
                        MaxWorks = @works,
                        GradingDate = @gradingdate,
                        CardinalGradingDescription = @cardinaldesc
                    WHERE
                        Id = @id",
                    new
                    {
                        id = entity.Id,
                        isDistributed = entity.IsDistributed,
                        name = entity.Name,
                        description = entity.Description,
                        allowSub = entity.AllowSubmissionFromDate,
                        duedate = entity.DueDate,
                        maxgrade = entity.MaxGrade,
                        mmaxgrade = entity.MoodleMaxGrade,
                        works = entity.MaxWorks,
                        gradingdate = entity.GradingDate,
                        cardinaldesc = entity.CardinalGradingDescription
                    });
        }

        public IEnumerable<Task> All()
        {
            throw new System.NotImplementedException();
        }

        public void Delete(Task entity)
        {
            throw new System.NotImplementedException();
        }
    }
}
