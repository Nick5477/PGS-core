﻿namespace Infrastructure.Db.Repositories
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Dapper;
    using Domain.Entities;
    using Domain.Repositories;
    using Transactions;

    public class SubmissionRepository : IRepository<Submission>
    {
        private readonly IDbTransactionProvider _transactionProvider;

        public SubmissionRepository(IDbTransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider;
        }
        public void Add(Submission entity)
        {
            entity.InitializeId();

            _transactionProvider
                .CurrentTransaction
                .Connection
                .Execute(@"
                INSERT INTO Submission
                (
                    Id,
                    TextAnswer,
                    FileAnswer,
                    SummaryGrade,
                    Priority,
                    TaskId,
                    UserId,
                    UploadTime
                )
                VALUES
                (
                    @id,
                    @text,
                    @file,
                    @grade,
                    @priority,
                    @task,
                    @user,
                    @date
                );", new
                {
                    id = entity.Id,
                    text = entity.TextAnswer,
                    file = entity.FileAnswer.FileName,
                    grade = entity.SummaryGrade,
                    priority = entity.Priority,
                    task = entity.Task.Id,
                    user = entity.User.Id,
                    date = entity.UploadTime
                });
        }

        public Submission Get(string id)
        {
            return _transactionProvider
                .CurrentTransaction
                .Connection
                .Query<Submission, File, Task, User, Submission>(@"
                    SELECT 
                        s.Id,
                        s.TextAnswer,
                        s.SummaryGrade,
                        s.Priority,
                        s.UploadTime,
                        f.Id,
                        f.FileName,
                        f.Path,
                        t.Id,
                        t.MoodleId,
                        t.Name,
                        t.Description,
                        t.AllowSubmissionFromDate,
                        t.DueDate,
                        t.MaxGrade,
                        t.MoodleMaxGrade,
                        t.IsDistributed,
                        t.MaxWorks,
                        u.Id,
                        u.Nickname
                FROM Submission s
                JOIN File f ON f.FileName=s.FileAnswer                
                JOIN User u ON u.Id=s.UserId
                JOIN Task t ON t.Id=s.TaskId
                WHERE s.Id = @subId",
                    (submission, file, task, user) =>
                    {
                        submission.GetType().GetProperty("FileAnswer").SetValue(submission, file);
                        submission.GetType().GetProperty("Task").SetValue(submission, task);
                        submission.GetType().GetProperty("User").SetValue(submission, user);

                        return submission;
                    },
                    new
                    {
                        subId = id
                    })
                    .FirstOrDefault();
        }

        public void Update(Submission entity)
        {
            _transactionProvider
                .CurrentTransaction
                .Connection
                .Execute(@"
                UPDATE Submission
                SET SummaryGrade=@grade
                WHERE Id=@id",
                new
                {
                    id=entity.Id,
                    grade = entity.SummaryGrade
                });
        }

        public IEnumerable<Submission> All()
        {
            throw new System.NotImplementedException();
        }

        public void Delete(Submission entity)
        {
            throw new System.NotImplementedException();
        }
    }
}
