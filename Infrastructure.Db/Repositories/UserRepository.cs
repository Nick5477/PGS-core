﻿namespace Infrastructure.Db.Repositories
{
    using System;
    using System.Collections.Generic;
    using Domain.Entities;
    using Domain.Repositories;
    using Transactions;
    using Dapper;
    using System.Reflection;
    using System.Linq;

    public class UserRepository : IRepository<User>
    {
        private readonly IDbTransactionProvider _transactionProvider;

        public UserRepository(IDbTransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider ?? throw new ArgumentNullException(nameof(transactionProvider));
        }
        public void Add(User entity)
        {
            entity.InitializeId();
            _transactionProvider.CurrentTransaction.Connection.Execute(@"
                INSERT INTO User
                (
                    Id,
                    Nickname,
                    RoleId,
                    Name,
                    LastName,
                    Patronymic,
                    MoodleId
                )
                VALUES
                (
                    @Id,
                    @Nickname,
                    @Role,
                    @name,
                    @lastname,
                    @patr,
                    @moodleid
                );", new
                {
                    Id = entity.Id,
                    Nickname = entity.Nickname,
                    Role = entity.Role.Id,
                    name = entity.Name,
                    lastname = entity.LastName,
                    patr = entity.Patronymic,
                    moodleid = entity.MoodleId
                });
        }

        public User Get(string id)
        {
            return _transactionProvider
                .CurrentTransaction
                .Connection
                .Query<User, Role, User>(@"
                SELECT
                    u.Id,
                    u.Nickname,
                    r.Id,
                    r.Name
                FROM User u
                JOIN Role r ON u.RoleId=r.Id
                WHERE Nickname=@nickname OR u.Id=@nickname",
                    (user, role) =>
                    {
                        user.GetType().GetProperty("Role").SetValue(user, role);

                        return user;
                    },
                    new
                    {
                        nickname = id
                    })
                    .FirstOrDefault();
        }

        public void Update(User entity)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<User> All()
        {
            return _transactionProvider
                .CurrentTransaction
                .Connection
                .Query<User, Role, User>(@"
                SELECT
                    u.Id,
                    u.Nickname,
                    u.Name,
                    u.LastName,
                    u.Patronymic,
                    r.Id,
                    r.Name
                FROM User u
                JOIN Role r ON u.RoleId=r.Id",
                    (user, role) =>
                    {
                        user.GetType().GetProperty("Role").SetValue(user, role);

                        return user;
                    });
        }

        public void Delete(User entity)
        {
            throw new System.NotImplementedException();
        }
    }
}
