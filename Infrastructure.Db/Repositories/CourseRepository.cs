﻿namespace Infrastructure.Db.Repositories
{
    using System;
    using System.Collections.Generic;
    using Dapper;
    using Domain.Entities;
    using Domain.Repositories;
    using Transactions;

    public class CourseRepository : IRepository<Course>
    {
        private readonly IDbTransactionProvider _transactionProvider;

        public CourseRepository(IDbTransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider ?? throw new ArgumentNullException(nameof(transactionProvider));
        }

        public void Add(Course entity)
        {
            entity.InitializeId();
            _transactionProvider
                .CurrentTransaction
                .Connection
                .Execute(@"
                INSERT INTO Course
                (
                    Id,
                    MoodleId,
                    FullName,
                    ShortName
                )
                VALUES
                (
                    @id,
                    @moodleid,
                    @fullname,
                    @shortname
                );", new
                {
                    id = entity.Id,
                    moodleid = entity.MoodleId,
                    fullname = entity.FullName,
                    shortname = entity.ShortName
                });
        }

        public Course Get(string id)
        {
            return _transactionProvider
                .CurrentTransaction
                .Connection
                .QueryFirstOrDefault<Course>(@"
                SELECT 
                    Id,
                    MoodleId,
                    FullName,
                    ShortName
                FROM Course
                WHERE Id=@courseid",
                    new
                    {
                        courseid = id
                    });
        }

        public void Update(Course entity)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Course> All()
        {
            return _transactionProvider
                .CurrentTransaction
                .Connection
                .Query<Course>(@"
                SELECT 
                    Id,
                    MoodleId,
                    FullName,
                    ShortName
                FROM Course");
        }

        public void Delete(Course entity)
        {
            throw new System.NotImplementedException();
        }
    }
}
