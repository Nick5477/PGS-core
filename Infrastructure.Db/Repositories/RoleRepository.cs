﻿namespace Infrastructure.Db.Repositories
{
    using System.Collections.Generic;
    using Domain.Entities;
    using Domain.Repositories;
    using Transactions;
    using System.Linq;
    using Dapper;

    public class RoleRepository : IRepository<Role>
    {
        private readonly IDbTransactionProvider _transactionProvider;

        public RoleRepository(IDbTransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider;
        }

        public void Add(Role entity)
        {
            throw new System.NotImplementedException();
        }

        public Role Get(string id)
        {
            return _transactionProvider
                .CurrentTransaction
                .Connection
                .Query<Role>(@"
                    SELECT
                        Id,
                        Name
                    FROM Role
                    WHERE Name=@name",
                    new
                    {
                        name = id
                    })
                .FirstOrDefault();
        }

        public void Update(Role entity)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Role> All()
        {
            throw new System.NotImplementedException();
        }

        public void Delete(Role entity)
        {
            throw new System.NotImplementedException();
        }
    }
}
