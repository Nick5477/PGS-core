﻿namespace Infrastructure.Db.Postgres
{
    using Npgsql;

    public static class PostgresConnectionProvider
    {
        private static string _moodleDomain = "localhost";

        private static string _moodlePort = "15432";

        private static string _dbUserName = "nick5477";

        private static string _dbUserPassword = "80bf476104e6415a94533c3f2dc818f6";

        private static string _dbName = "moodle";

        public static NpgsqlConnection CreateConnection()
        {
            return new NpgsqlConnection($"Server ={_moodleDomain}; Port ={_moodlePort}; User Id={_dbUserName};Password={_dbUserPassword};Database={_dbName};");
        }
    }
}
