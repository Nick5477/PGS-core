﻿namespace Infrastructure.Db.Mysql
{
    using MySql.Data.MySqlClient;

    public static class MysqlConnectionProvider
    {
        private static string _moodleDomain = "localhost";
        private static string _dbUserName = "root";
        private static string _dbUserPassword = "1234";
        private static string _dbName = "moodle";

        public static MySqlConnection CreateConnection()
        {
            return new MySqlConnection(
                $"server={_moodleDomain};uid={_dbUserName};pwd={_dbUserPassword};database={_dbName};SslMode=none;charset=utf8;");
        }
    }
}
