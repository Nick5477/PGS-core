﻿namespace Infrastructure.Db.Sqlite
{
    using System.Data;
    using Commands;
    using Transactions;
    using Microsoft.Data.Sqlite;

    public class SqliteCommandFactory : DbCommandFactoryBase
    {
        public SqliteCommandFactory(IDbTransactionProvider dbTransactionProvider) : base(dbTransactionProvider)
        {
        }

        protected override IDbCommand CreateCommand()
        {
            return new SqliteCommand();
        }
    }
}
