﻿namespace Infrastructure.Db.UnitOfWork
{
    using System;
    using System.Data;
    using Domain.UnitOfWork;
    using Transactions;

    public class DbUnitOfWork : IUnitOfWork
    {
        private readonly IDbConnection _connection;
        private IDbTransaction _transaction;

        public DbUnitOfWork(IDbConnection connection)
        {
            _connection = connection ?? throw new ArgumentNullException(nameof(connection));

            _connection.Open();
            _transaction = _connection.BeginTransaction();

            DbTransactionContext.Bind(_transaction);
        }

        public void Dispose()
        {
            DbTransactionContext.Unbind();
            _transaction.Dispose();
            _transaction = null;
            _connection.Dispose();
        }

        public void Commit()
        {
            _transaction.Commit();
        }
    }
}
