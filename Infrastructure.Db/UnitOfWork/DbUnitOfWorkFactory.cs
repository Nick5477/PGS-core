﻿namespace Infrastructure.Db.UnitOfWork
{
    using System.Data;
    using Domain.UnitOfWork;
    using Connections;

    public class DbUnitOfWorkFactory : IUnitOfWorkFactory
    {
        private readonly IDbConnectionFactory _connectionFactory;

        public DbUnitOfWorkFactory(IDbConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public IUnitOfWork Create(IsolationLevel isolationLevel)
        {
            return new DbUnitOfWork(_connectionFactory.OpenConnection());
        }

        public IUnitOfWork Create()
        {
            return Create(IsolationLevel.RepeatableRead);
        }
    }
}
