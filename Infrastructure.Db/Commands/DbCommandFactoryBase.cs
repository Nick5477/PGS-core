﻿namespace Infrastructure.Db.Commands
{
    using System;
    using System.Data;
    using Infrastructure.Db.Transactions;

    public abstract class DbCommandFactoryBase : IDbCommandFactory
    {
        protected readonly IDbTransactionProvider _transactionProvider;

        public DbCommandFactoryBase(IDbTransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider ?? throw new ArgumentNullException(nameof(transactionProvider));
        }

        protected abstract IDbCommand CreateCommand();

        public IDbCommand Create()
        {
            IDbTransaction currentTransaction = _transactionProvider.CurrentTransaction;
            IDbCommand command = CreateCommand();

            command.Connection = currentTransaction.Connection;
            command.Transaction = currentTransaction;

            return command;
        }
    }
}
