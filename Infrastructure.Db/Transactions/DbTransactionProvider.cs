﻿namespace Infrastructure.Db.Transactions
{
    using System;
    using System.Data;

    public class DbTransactionProvider : IDbTransactionProvider
    {
        public IDbTransaction CurrentTransaction
        {
            get
            {
                if (DbTransactionContext.HasBindedTransaction)
                    return DbTransactionContext.CurrentTransaction;

                throw new InvalidOperationException("Database access logic can not be used, if transaction not started");
            }
        }
    }
}
