﻿function check() {
    if (checkSelectedRadioButtons()) {
        document.getElementById('btnGrade').disabled = false;
    } else {
        document.getElementById('btnGrade').disabled = true;
    }
}

function checkSelectedRadioButtons() {
    var buttons = document.getElementsByTagName('input');
    var radioGroups = {};
    for (var i = 0; i < buttons.length; i++) {
        if (buttons[i].type === 'radio') {
            radioGroups[buttons[i].name] = false;    
        }
    }
    for (var i = 0; i < buttons.length; i++) {
        if (buttons[i].type === 'radio' && buttons[i].checked) {
            radioGroups[buttons[i].name] = true; 
        }
    }

    for (var group in radioGroups) {
        if (!radioGroups[group]) {
            return false;
        }
    }
    return true;
}