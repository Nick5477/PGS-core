﻿function addRubric() {
    var num = $('.rubric').length + 1;
    var htmlRubric = '<div id=rubric' + num +'> \r\n \
        <br/><br/><div class="row"> \r\n \
            <label for="rubric'+ num +'_name">Название рубрики</label><br /> \r\n \
            <input class="rubric" id="rubric'+ num + '_name" type="text" placeholder="Название рубрики" name="rubric' + num +'_name" /> \r\n \
        </div> \r\n \
        <div class="row"> \r\n \
            <label for="rubric'+ num + '_name">Описание рубрики</label><br /> \r\n \
            <textarea name="rubric'+ num + '_description" cols="40" rows="5" placeholder="Описание критерия" id="rubric' + num +'_description"></textarea> \r\n \
        </div> \r\n \
        <div class="row" id="rubric'+ num +'_level1"> \r\n \
            <label>Уровень</label><br /> \r\n \
            <textarea class="rubric'+ num + '" id="rubric' + num + '_level1_description" name="rubric' + num +'_level1_description" placeholder="Описание уровня" cols="22" rows="3"></textarea><br /> \r\n \
            <input type="number" id="rubric'+ num + '_level1_points" value="0" disabled name="rubric' + num +'_level1_points" /> \r\n \
            <label> баллов</label> \r\n \
        </div> \r\n \
        <div class="row" id="rubric'+ num +'_level2"> \r\n \
            <label>Уровень</label><br /> \r\n \
            <textarea class="rubric'+ num + '" id="rubric' + num + '_level2_description" name="rubric' + num +'_level2_description" placeholder="Описание уровня" cols="22" rows="3"></textarea><br /> \r\n \
            <input type="number" id="rubric'+ num + '_level2_points" value="1" name="rubric' + num +'_level2_points" /> \r\n \
            <label> баллов</label> \r\n \
        </div> \r\n \
        <div class="row" id="rubric'+ num +'_level3"> \r\n \
            <label>Уровень</label><br /> \r\n \
            <textarea class="rubric'+ num + '" id="rubric' + num + '_level3_description" name="rubric' + num +'_level3_description" placeholder="Описание уровня" cols="22" rows="3"></textarea><br /> \r\n \
            <input type="number" id="rubric'+ num + '_level3_points" value="2" name="rubric' + num +'_level3_points" /> \r\n \
            <label> баллов</label> \r\n \
            <a class="btn btn-default btn-xs" onclick="deleteRubricLevel(this.id)" id="btn_delete_rubric' + num +'_level3"><span class="glyphicon glyphicon-remove"></span> Удалить уровень</a> \r\n \
        </div> \r\n \
        <div class="row" id="divRubric'+ num +'AddLevel"> \r\n \
            <br /><a class="btn btn-default btn-sm" id="btn_rubric'+ num +'_add_level" onclick="addLevel(this.id)"><span class="glyphicon glyphicon-plus"></span> Добавить уровень</a> \r\n \
        </div> \r\n \
        <div class="row"> \r\n \
            <br/><a class="btn btn-default btn-sm" onclick="deleteRubric(this.id)" id="btn_delete_rubric'+ num + '"><span class="glyphicon glyphicon-remove"></span> Удалить рубрику</a> \r\n \
        </div> \r\n \
        </div>';
    $('#divAddRubric').before(htmlRubric);
}

function addLevel(buttonId) {
    var strArray = buttonId.split('_');
    var num = strArray[1].replace('rubric', '');
    var levelNumber = $('.rubric' + num).length + 1;
    var htmlLevel = '<div class="row" id="rubric' + num + '_level' + levelNumber + '"> \r\n \
            <label>Уровень</label><br /> \r\n \
            <textarea class="rubric'+ num + '" id="rubric' + num + '_level' + levelNumber + '_description" name="rubric' + num + '_level' + levelNumber + '_description" placeholder="Описание уровня" cols="22" rows="3"></textarea><br /> \r\n \
            <input type="number" id="rubric'+ num + '_level' + levelNumber + '_points" name="rubric' + num + '_level' + levelNumber + '_points" /> \r\n \
            <label> баллов</label> \r\n \
            <a class="btn btn-default btn-xs" onclick="deleteRubricLevel(this.id)" id="btn_delete_rubric' + num + '_level' + levelNumber + '"><span class="glyphicon glyphicon-remove"></span> Удалить уровень</a> \r\n \
        </div> \r\n';
    $('#divRubric' + num + 'AddLevel').before(htmlLevel);
}

function deleteRubric(buttonId) {
    var strArray = buttonId.split('_');
    $('#' + strArray[2]).remove();
}

function deleteRubricLevel(buttonId) {
    var strArray = buttonId.split('_');
    $('#' + strArray[2] + '_' + strArray[3]).remove();
}