namespace WebApp.Controllers.Rubric
{
    using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Domain.Entities;
    using Domain.Services;
    using Domain.Services.Rubric;
    using Domain.Services.Task;
    using Domain.UnitOfWork;

    using Microsoft.AspNetCore.Mvc;

    using WebApp.ViewModels;

    public class RubricController : Controller
    {
        private readonly IEntityService<Rubric> _rubricEntityService;

        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        private readonly IEntityService<Task> _taskEntityService;

        private readonly IRubricService _rubricService;

        private readonly IMapper _mapper;

        private readonly ITaskService _taskService;

        public RubricController(
            IEntityService<Rubric> rubricEntityService,
            IUnitOfWorkFactory unitOfWorkFactory,
            IEntityService<Task> taskEntityService,
            IRubricService rubricService,
            IMapper mapper,
            ITaskService taskService)
        {
            _rubricEntityService = rubricEntityService;
            _unitOfWorkFactory = unitOfWorkFactory;
            _taskEntityService = taskEntityService;
            _rubricService = rubricService;
            _mapper = mapper;
            _taskService = taskService;
        }

        [HttpGet]
        public IActionResult Get(string id)
        {
            LargeRubricViewModel model = new LargeRubricViewModel();
            using (IUnitOfWork unitOfWork = _unitOfWorkFactory.Create())
            {
                var task = _taskEntityService.Get(id);
                model.TaskGradingDescription = task.CardinalGradingDescription;

                var rubrics = _rubricService.GetRubricByTask(id);
                foreach (var rubric in rubrics)
                {
                    model.Rubrics.Add(_mapper.Map<RubricViewModel>(rubric));
                }

                if (rubrics.Count() != 0)
                {
                    ViewBag.Title = $"�������������� ������ ��� ������� {task.Name}";
                }
                else
                {
                    ViewBag.Title = "�������� ������";
                }

                unitOfWork.Commit();
            }

            model.TaskId = id;
            
            return View(model);
        }

        [HttpPost]
        public IActionResult Get(LargeRubricViewModel viewModel)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var task = _taskEntityService.Get(viewModel.TaskId);
                task.CardinalGradingDescription = Request.Form["gradingDescription"];
                
                var rubrics = _rubricService.GetRubricByTask(task.Id);
                if (rubrics.Count() != 0)
                {
                    foreach (var rubric in rubrics)
                    {
                        _rubricEntityService.Delete(rubric.Id);
                    }
                }

                rubrics = CreateRubrics(task);
                foreach (var rubric in rubrics)
                {
                    _rubricEntityService.Add(rubric);
                }

                task.MaxGrade = rubrics.Sum(r => r.Levels.Max(rl => rl.Points));
                _taskService.Update(task);

                unitOfWork.Commit();
            }
            return RedirectToRoute(new
            {
                controller = "Task",
                action = "Get",
                id = viewModel.TaskId
            });
        }

        private IEnumerable<Rubric> CreateRubrics(Task task)
        {
            Dictionary<string, Rubric> rubrics = new Dictionary<string, Rubric>();
            Dictionary<string, RubricLevel> globalRubricLevels = new Dictionary<string, RubricLevel>();
            foreach (var element in Request.Form)
            {
                if (element.Key.Contains("rubric"))
                {
                    var stringArray = element.Key.Split('_');

                    if (!rubrics.ContainsKey(stringArray[0]))
                    {
                        var newRubric = new Rubric()
                        {
                            Task = task
                        };
                        newRubric.InitializeId();
                        rubrics.Add(
                            stringArray[0],
                            newRubric);
                    }

                    if (stringArray.Length == 2)
                    {
                        switch (stringArray[1])
                        {
                            case "description":
                                rubrics[stringArray[0]].Description = element.Value;
                                break;
                            case "name":
                                rubrics[stringArray[0]].Name = element.Value;
                                break;
                        }
                    }

                    if (stringArray.Length == 3)
                    {
                        if (!globalRubricLevels.ContainsKey($"{stringArray[0]}_{stringArray[1]}"))
                        {
                            var newRubricLevel = new RubricLevel();
                            newRubricLevel.InitializeId();
                            globalRubricLevels.Add(
                                $"{stringArray[0]}_{stringArray[1]}",
                                newRubricLevel);
                        }

                        switch (stringArray[2])
                        {
                            case "description":
                                globalRubricLevels[$"{stringArray[0]}_{stringArray[1]}"].Description = element.Value;
                                break;
                            case "points":
                                globalRubricLevels[$"{stringArray[0]}_{stringArray[1]}"].Points = int.Parse(element.Value);
                                break;
                        }
                    }
                }
            }

            foreach (var rubricLevel in globalRubricLevels)
            {
                rubrics[rubricLevel.Key.Split('_')[0]].Levels.Add(rubricLevel.Value);
            }

            return rubrics.Select(kv => kv.Value);
        }
    }
}