﻿namespace WebApp.Controllers.Rubric.Profiles
{
    using AutoMapper;

    using Domain.Entities;

    using WebApp.ViewModels;

    public class RubricProfile : Profile
    {
        public RubricProfile()
        {
            CreateMap<Rubric, RubricViewModel>();
        }
    }
}
