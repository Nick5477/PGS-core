namespace WebApp.Controllers.Course
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Domain.Entities;
    using Domain.Services;
    using Domain.Services.Course;
    using Domain.Services.Task;
    using Domain.Services.User;
    using Domain.UnitOfWork;

    using Microsoft.AspNetCore.Mvc;

    using WebApp.ViewModels;

    public class CourseController : Controller
    {
        private readonly IEntityService<Course> _courseEntityService;
        private readonly ICourseService _courseService;
        private readonly IUserService _userService;
        private readonly IEntityService<Task> _taskEntityService;
        private readonly ITaskService _taskService;
        private readonly IMapper _mapper;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;


        public CourseController(
            IUnitOfWorkFactory unitOfWorkFactory,
            IMapper mapper,
            IEntityService<Course> courseEntityService,
            ICourseService courseService,
            IUserService userService,
            IEntityService<Task> taskEntityService,
            ITaskService taskService)
        {
            _courseEntityService = courseEntityService;
            _courseService = courseService;
            _userService = userService;
            _taskEntityService = taskEntityService;
            _taskService = taskService;
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _unitOfWorkFactory = unitOfWorkFactory ?? throw new ArgumentNullException(nameof(unitOfWorkFactory));
        }
        [HttpGet]
        public IActionResult List()
        {
            List<Course> courses = new List<Course>();
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                courses = GetCoursesByUsername(User.Identity.Name).ToList();

                unitOfWork.Commit();
            }
                
            return View(_mapper.Map<IEnumerable<CourseViewModel>>(courses));
        }

        [HttpPost]
        public IActionResult List(string username)
        {
            using (IUnitOfWork unitOfWork = _unitOfWorkFactory.Create())
            {
                if (!User.IsInRole("teacher"))
                {
                    var courseList = GetCoursesByUsername(User.Identity.Name);
                    return View(_mapper.Map<IEnumerable<CourseViewModel>>(courseList));
                }
                unitOfWork.Commit();
            }

            List<Course> resultList = new List<Course>();
            IEnumerable<Course> courses;
            try
            {
                courses = _courseService.GetCoursesFromMoodle();
            }
            catch (Exception ex)
            {
                using (var unitOfWork = _unitOfWorkFactory.Create())
                {
                    courses = GetCoursesByUsername(User.Identity.Name);

                    unitOfWork.Commit();
                }
            }
            
            foreach (var course in courses)
            {
                Course courseFromDB;
                using (IUnitOfWork unitOfWork = _unitOfWorkFactory.Create())
                {
                    courseFromDB = _courseService.GetCourseByMoodleId(course.MoodleId);
                    unitOfWork.Commit();
                }
                if (courseFromDB == null)
                {
                    using (IUnitOfWork unitOfWork = _unitOfWorkFactory.Create())
                    {
                        course.InitializeId();
                        _courseEntityService.Add(course);
                        resultList.Add(course);
                        unitOfWork.Commit();
                    }
                }
                else
                {
                    resultList.Add(courseFromDB);
                }
                using (IUnitOfWork unitOfWork = _unitOfWorkFactory.Create())
                {
                    try
                    {
                        BindLocalUsersToCourse(resultList.Last());
                    }
                    catch (Exception ex)
                    {
                        
                    }

                    unitOfWork.Commit();
                }
                using (IUnitOfWork unitOfWork = _unitOfWorkFactory.Create())
                {
                    try
                    {
                        var tasks = _taskService.GetTasksFromMoodle(course.MoodleId);
                        AddTasks(tasks, resultList.Last());
                    }
                    catch (Exception ex)
                    {
                        
                    }
                    
                    unitOfWork.Commit();
                }
            }
            return View(_mapper.Map<IEnumerable<CourseViewModel>>(resultList));
        }

        public IActionResult Get(string id)
        {
            IEnumerable<Task> tasks;
            Course course;
            using (IUnitOfWork unitOfWork = _unitOfWorkFactory.Create())
            {
                tasks=_taskService.GetTasksByCourseId(id);
                course = _courseEntityService.Get(id);

                unitOfWork.Commit();
            }
            ViewBag.Title = $"{course.FullName} - �������";
            return View(_mapper.Map<IEnumerable<TaskViewModel>>(tasks));
        }

        private void BindLocalUsersToCourse(Course course)
        {
            List<User> users = _userService.GetAllLocalUsers().ToList();

            foreach (var user in users)
            {
                if (_courseService.IsBindedUserToCourseOnMoodle(user, course))
                {
                    if (!_courseService.IsBindedUserToLocalCourse(user, course))
                    {
                        _courseService.BindUserToCourse(user,course);
                    }
                }
            }
        }

        private void AddTasks(IEnumerable<Task> tasks, Course course)
        {
            foreach (var task in tasks)
            {
                if (_taskService.GetTaskByMoodleId(task.MoodleId) == null)
                {
                    task.SetCourse(course);
                    _taskEntityService.Add(task);
                }
            }
        }

        private IEnumerable<Course> GetCoursesByUsername(string username)
        {
            return _courseService.GetCoursesByUsername(username);
        }
    }
}