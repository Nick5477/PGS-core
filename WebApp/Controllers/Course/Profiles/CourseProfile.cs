﻿namespace WebApp.Controllers.Course.Profiles
{
    using AutoMapper;
    using ViewModels;

    public class CourseProfile : Profile
    {
        public CourseProfile()
        {
            CreateMap<Domain.Entities.Course, CourseViewModel>();
        }
    }
}
