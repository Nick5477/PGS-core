namespace WebApp.Controllers.File
{
    using Domain.Services;
    using Domain.UnitOfWork;
    using Microsoft.AspNetCore.Mvc;

    public class FileController : Controller
    {
        private readonly IEntityService<Domain.Entities.File> _fileService;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public FileController(
            IEntityService<Domain.Entities.File> fileService,
            IUnitOfWorkFactory unitOfWorkFactory)
        {
            _fileService = fileService;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public FileResult Download(string id, int number)
        {
            Domain.Entities.File file = null;
            using (IUnitOfWork unitOfWork = _unitOfWorkFactory.Create())
            {
                file = _fileService.Get(id);
                unitOfWork.Commit();
            }
            byte[] fileBytes = System.IO.File.ReadAllBytes(file.Path);
            return File(
                fileBytes,
                "application/x-msdownload",
                $"������ {number}{file.FileName.Substring(file.FileName.LastIndexOf("."))}");
        }
    }
}