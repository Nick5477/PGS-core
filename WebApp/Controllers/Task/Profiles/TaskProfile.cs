﻿namespace WebApp.Controllers.Task.Profiles
{
    using AutoMapper;
    using WebApp.ViewModels;

    public class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<Domain.Entities.Task, TaskViewModel>();

            CreateMap<Domain.Entities.Task, LargeTaskViewModel>();

            CreateMap<Domain.Entities.Task, OrdinalGradeTaskViewModel>();

            CreateMap<Domain.Entities.Task, GradesTaskViewModel>();
        }
    }
}
