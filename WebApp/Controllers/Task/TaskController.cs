namespace WebApp.Controllers.Task
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Domain.Entities;
    using Domain.Services;
    using Domain.Services.DistributionUnit;
    using Domain.Services.Submission;
    using Domain.Services.Task;
    using Domain.Services.User;
    using Domain.UnitOfWork;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using ViewModels;
    using Domain.Services.OrdinalGrade;
    using Domain.Services.CardinalGrade;
    using Domain.Services.Rubric;

    public class TaskController : Controller
    {
        private readonly IEntityService<Domain.Entities.Task> _entityTaskService;
        private readonly IMapper _mapper;
        private readonly ISubmissionService _submissionService;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IEntityService<Domain.Entities.File> _fileEntityService;
        private readonly IHostingEnvironment _appEnvironment;
        private readonly IEntityService<Submission> _submissionEntityService;
        private readonly IEntityService<User> _userEntityService;
        private readonly ITaskService _taskService;
        private readonly IDistributionService _distributionService;
        private readonly IUserService _userService;
        private readonly IEntityService<OrdinalGrade> _ordinalGradeEntityService;
        private readonly IOrdinalGradeService _ordinalGradeService;
        private readonly ICardinalGradeService _cardinalGradeService;

        private readonly IRubricService _rubricService;

        public TaskController(
            IEntityService<Domain.Entities.Task> entityTaskService,
            IMapper mapper,
            ISubmissionService submissionService,
            IUnitOfWorkFactory unitOfWorkFactory,
            IEntityService<Domain.Entities.File> fileEntityService,
            IHostingEnvironment appEnvironment,
            IEntityService<Submission> submissionEntityService,
            IEntityService<User> userEntityService,
            ITaskService taskService,
            IDistributionService distributionService,
            IUserService userService,
            IEntityService<OrdinalGrade> ordinalGradeEntityService,
            IOrdinalGradeService ordinalGradeService,
            ICardinalGradeService cardinalGradeService,
            IRubricService rubricService)
        {
            _entityTaskService = entityTaskService;
            _mapper = mapper;
            _submissionService = submissionService;
            _unitOfWorkFactory = unitOfWorkFactory;
            _fileEntityService = fileEntityService;
            _appEnvironment = appEnvironment;
            _submissionEntityService = submissionEntityService;
            _userEntityService = userEntityService;
            _taskService = taskService;
            _distributionService = distributionService;
            _userService = userService;
            _ordinalGradeEntityService = ordinalGradeEntityService;
            _ordinalGradeService = ordinalGradeService;
            _cardinalGradeService = cardinalGradeService;
            _rubricService = rubricService;
        }

        [HttpGet]
        public IActionResult Get(string id)
        {
            LargeTaskViewModel viewModel;
            
            using (IUnitOfWork unitOfWork=_unitOfWorkFactory.Create())
            {
                Domain.Entities.Task task = _entityTaskService.Get(id);
                ViewBag.Title = $"������� {task.Name}";

                List<Submission> submissions = null;
                viewModel = _mapper.Map<LargeTaskViewModel>(task);

                //���� ������ ����� ���������, � ������ ��� �� ������������
                if (DateTime.UtcNow > task.AllowSubmissionFromDate
                    && task.IsDistributed != true)
                {
                    CreateDistribution(task);
                }

                if (!HttpContext.User.IsInRole("teacher"))
                {
                    var distributions = _distributionService.GetDistributionsByUsernameAndTask(User.Identity.Name, task);
                    submissions = _submissionService.GetSubmissionsByDistributions(distributions);

                    viewModel.Submissions = submissions?.ToList();

                    Submission userSubmission =
                        _submissionService.GetUserSubmission(
                            HttpContext.User.Identity.Name,
                            id);
                    viewModel.IsAttachedSubmission = userSubmission != null;
                }

                viewModel.CourseId = task.Course.Id;
                var user = _userEntityService.Get(HttpContext.User.Identity.Name);

                if (!HttpContext.User.IsInRole("teacher"))
                {
                    if (task.PeerGrading == PeerGradingEnum.Ordinal)
                    {
                        viewModel.IsGradedSubmissions =
                            _ordinalGradeService
                                .IsGradedSubmissionsByUser(
                                    user.Id,
                                    task.Id);
                    }
                    else
                    {
                        viewModel.CardinalGrades =
                            _cardinalGradeService.GetCardinalGradesByTaskAndUser(task, user).ToList();
                        var rubrics = _rubricService.GetRubricByTask(task.Id);
                        viewModel.Rubric = rubrics.ToList();
                    }
                }

                unitOfWork.Commit();
            }
            return View(viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> AddFile(IFormFile uploadedFile, string textAnswer, string taskId)
        {
            if (uploadedFile != null)
            {
                string path = $"{_appEnvironment.WebRootPath}\\Files\\{DateTime.UtcNow.Date.ToString("yyyyMMdd")}";
                Directory.CreateDirectory(path);

                // ��������� ���� � ����� Files � �������� wwwroot
                var file = new Domain.Entities.File(path, uploadedFile.FileName);

                using (var fileStream = new FileStream(file.Path, FileMode.Create))
                {
                    await uploadedFile.CopyToAsync(fileStream);
                }

                using (IUnitOfWork unitOfWork = _unitOfWorkFactory.Create())
                {
                    _fileEntityService.Add(file);

                    unitOfWork.Commit();
                }
                User user;
                Domain.Entities.Task task;
                using (IUnitOfWork unitOfWork = _unitOfWorkFactory.Create())
                {
                    user = _userEntityService.Get(HttpContext.User.Identity.Name);
                    task = _entityTaskService.Get(taskId);

                    unitOfWork.Commit();
                }

                using (IUnitOfWork unitOfWork = _unitOfWorkFactory.Create())
                {
                    _submissionEntityService.Add(
                        new Domain.Entities.Submission(
                            task,
                            textAnswer,
                            file,
                            DateTime.UtcNow,
                            user
                        ));

                    unitOfWork.Commit();
                }
            }
            return RedirectToRoute(new
            {
                controller = "Task",
                action = "Get",
                id = taskId
            });
        }

        [HttpGet]
        [Route("Task/Settings/{task}")]
        public IActionResult Settings(string task)
        {
            ViewBag.Title = "���������";

            
            TaskViewModel model = new TaskViewModel();
            using (IUnitOfWork unitOfWork = _unitOfWorkFactory.Create())
            {
                var taskObject = _entityTaskService.Get(task);
                model = _mapper.Map<TaskViewModel>(taskObject);
                model.CourseName = taskObject.Course?.FullName;

                unitOfWork.Commit();    
            }
            return View(model);
        }

        [HttpPost]
        [Route("Task/Settings/{task}")]
        public IActionResult Settings(TaskViewModel model)
        {
            using (IUnitOfWork unitOfWork = _unitOfWorkFactory.Create())
            {
                var task = _entityTaskService.Get(model.Id);
                task.MaxWorks = int.Parse(Request.Form["maxworks"]);
                _taskService.Update(task);
                _distributionService.DeleteDistributionUnitsByTaskId(task.Id);
                CreateDistribution(task);

                unitOfWork.Commit();    
            }
            return RedirectToRoute(new
            {
                controller = "Task",
                action = "Get",
                id = model.Id
            });
        }

        private void CreateDistribution(Domain.Entities.Task task)
        {
            IEnumerable<User> users = _userService.GetUsersByCourse(task.Course.Id);
            Dictionary<int, string> dict = new Dictionary<int, string>();
            int i = 0;

            foreach (var user in users)
            {
                if (user.Role.Id == Role.Keys.Student)
                {
                    dict.Add(i, user.Id);
                    i++;
                }
            }

            if (task.MaxWorks.HasValue)
            {
                _distributionService.CreateDistributions(dict.Count, task.MaxWorks.Value, dict, task.Id);
                task.IsDistributed = true;
                _taskService.Update(task);
            }
        }

        [HttpGet]
        [Route("Task/Grade/{taskId}")]
        public IActionResult Grade(string taskId)
        {
            Domain.Entities.Task task = null;
            List<Domain.Entities.Submission> submissions = null;
            List<DistributionUnit> distributions = null;

            using (IUnitOfWork unitOfWork = _unitOfWorkFactory.Create())
            {
                task = _entityTaskService.Get(taskId);
                distributions = _distributionService.GetDistributionsByUsernameAndTask(User.Identity.Name, task);
                submissions = _submissionService.GetSubmissionsByDistributions(distributions);
                unitOfWork.Commit();
            }

            ViewBag.Title = $"���������� ������� {task?.Name}";

            OrdinalGradeTaskViewModel viewModel = _mapper.Map<OrdinalGradeTaskViewModel>(task);
            viewModel.CourseName = task?.Course?.FullName;
            viewModel.Submissions = submissions;
            Dictionary<DistributionUnit, Domain.Entities.File> dict = new Dictionary<DistributionUnit, Domain.Entities.File>();
            foreach (var submission in submissions)
            {
                dict.Add(distributions.First(x => x.StudentId == submission.User.Id), submission.FileAnswer);
            }

            dict = dict.OrderBy(x => x.Key.GraderWorkNumber).ToDictionary(kv => kv.Key, kv => kv.Value);
            viewModel.DistributionFile = dict;
            if (viewModel.DistributionFile?.Count < 2)
            {
                return RedirectToRoute(
                    new
                    {
                        controller = "Task",
                        action = "ErrorGrading"
                    });
            }

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Grade(OrdinalGradeTaskViewModel taskViewModel)
        {
            List<OrdinalGrade> grades = new List<OrdinalGrade>();
            List<string> works = new List<string>();
            List<string> gradeValues = new List<string>();
            string controlId = "work1";
            string gradeControlId = "grade1";
            int i = 1;
            while (!string.IsNullOrEmpty(Request.Form[controlId]))
            {
                works.Add(Request.Form[controlId]);
                if (!string.IsNullOrWhiteSpace(Request.Form[gradeControlId]))
                {
                    gradeValues.Add(Request.Form[gradeControlId]);
                }
                i++;
                controlId = controlId.Remove(controlId.Length - 1);
                controlId += i.ToString();
                gradeControlId = gradeControlId.Remove(gradeControlId.Length - 1);
                gradeControlId += i.ToString();
            }

            using (IUnitOfWork unitOfWork = _unitOfWorkFactory.Create())
            {
                var task = _entityTaskService.Get(taskViewModel.Id);
                var distributions = _distributionService.GetDistributionsByUsernameAndTask(User.Identity.Name, task);
                var grader = _userEntityService.Get(distributions.First().GraderId);

                for (int j = 0; j < works.Count - 1; j++)
                {
                    var distribution1 = distributions
                        .First(d => d.GraderWorkNumber == int.Parse(works.ElementAt(j)));
                    var distribution2 = distributions
                        .First(d => d.GraderWorkNumber == int.Parse(works.ElementAt(j+1)));
                    var user1 = _userEntityService.Get(distribution1.StudentId);
                    var user2 = _userEntityService.Get(distribution2.StudentId);
                    double grade = 0;
                    switch (gradeValues.ElementAt(j))
                    {
                        case "������� �����, ���":
                            grade = 0.85;
                            break;
                        case "�����, ���":
                            grade = 0.65;
                            break;
                        case "������� �����, ���":
                            grade = 0.58;
                            break;
                        case "�������� �� ����� ������ �":
                            grade = 0.5;
                            break;
                    }
                    var ordinalGrade = new OrdinalGrade()
                    {
                        FirstStudent = user1,
                        SecondStudent = user2,
                        Grade = grade,
                        Grader = grader,
                        Task = task
                    };
                    ordinalGrade.InitializeId();
                    grades.Add(ordinalGrade);
                    ordinalGrade = new OrdinalGrade()
                    {
                        FirstStudent = user2,
                        SecondStudent = user1,
                        Grade = 1-grade,
                        Grader = grader,
                        Task = task
                    };
                    ordinalGrade.InitializeId();
                    grades.Add(ordinalGrade);
                }

                foreach (var ordinalGrade in grades)
                {
                    _ordinalGradeEntityService.Add(ordinalGrade);
                }

                unitOfWork.Commit();
            }
            return RedirectToRoute(new
            {
                controller = "Task",
                action = "Get",
                id = taskViewModel.Id
            });
        }

        [HttpGet]
        public IActionResult ErrorGrading()
        {
            ViewBag.Title = "������";
            return View();
        }

        [HttpGet]
        public IActionResult Grades(string id)
        {
            Domain.Entities.Task task = null;
            List<Submission> submissions = null;
            using (IUnitOfWork unitOfWork = _unitOfWorkFactory.Create())
            {
                task = _entityTaskService.Get(id);
                submissions = _submissionService.GetSubmissionsByTask(task);

                unitOfWork.Commit();
            }

            ViewBag.Title = $"������ �� ������� {task.Name}";
            GradesTaskViewModel model = _mapper.Map<GradesTaskViewModel>(task);
            model.CourseName = task.Course.FullName;
            model.Submissions = submissions;

            return View(model);
        }

        [HttpGet]
        public bool CalculateGrades(string id)
        {
            Domain.Entities.Task task;
            using (IUnitOfWork unitOfWork = _unitOfWorkFactory.Create())
            {
                task = _entityTaskService.Get(id);

                unitOfWork.Commit();
            }
            _ordinalGradeService.CalculateOrdinalGrades(task);

            return true;
        }
    }
}