﻿namespace WebApp.Controllers.Account
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using Domain.Commands;
    using Domain.Services;
    using Domain.Services.Course;
    using Domain.Services.User;
    using Domain.UnitOfWork;
    using Infrastructure.Db.Mysql;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
    using Microsoft.AspNetCore.Mvc;
    using MySql.Data.MySqlClient;
    using Newtonsoft.Json.Linq;
    using Models;
    using System.Linq;

    using Infrastructure.Db.Postgres;

    using Npgsql;

    public class AccountController : Controller
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private string _webService = "moodle_mobile_app";
        public string MoodleDomain => "movs.psu.ru:8080";


        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IUserService _userService;
        private readonly ICourseService _courseService;
        private readonly IEntityService<Domain.Entities.User> _userEntityService;
        private readonly IEntityService<Domain.Entities.Role> _roleService;

        public AccountController(
            ICommandBuilder commandBuilder,
            IUnitOfWorkFactory unitOfWorkFactory,
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            RoleManager<IdentityRole> roleManager,
            IUserService userService,
            ICourseService courseService,
            IEntityService<Domain.Entities.User> userEntityService,
            IEntityService<Domain.Entities.Role> roleService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _userService = userService;
            _courseService = courseService;
            _userEntityService = userEntityService;
            _roleService = roleService;
            _unitOfWorkFactory = unitOfWorkFactory ?? throw new ArgumentNullException(nameof(unitOfWorkFactory));
        }

        public IActionResult Login(string returnUrl)
        {
            ViewBag.returnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel loginModel, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                User user = null;
                var result = 
                    await _signInManager.PasswordSignInAsync(loginModel.UserName, loginModel.Password, true, false);
                if (result.Succeeded)
                {
                    // проверяем, принадлежит ли URL приложению
                    if (!String.IsNullOrEmpty(loginModel.ReturnUrl) && Url.IsLocalUrl(loginModel.ReturnUrl))
                    {
                        return Redirect(loginModel.ReturnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    user = await CreateNewUserByLoginModel(loginModel);
                }


                if (user != null)
                {
                    var role = await _roleManager.FindByIdAsync(user.Roles.First().RoleId);
                    CreateLocalUser(loginModel.UserName, role.Name);
                    BindNewUserToCourse(loginModel.UserName);
                    result = await _signInManager
                        .PasswordSignInAsync(
                            loginModel.UserName,
                            loginModel.Password,
                            true,
                            false);
                    if (String.IsNullOrEmpty(returnUrl))
                        return RedirectToAction("Index", "Home");

                    return Redirect(returnUrl);
                }
            }
            ViewBag.returnUrl = returnUrl;
            return View(loginModel);
        }

        private async Task<User> CreateNewUserByLoginModel(LoginModel loginModel)
        {
            string userToken = await GetAuthToken(loginModel);
            if (userToken != null)
            {
                User user = new User()
                {
                    Id = userToken,
                    UserName = loginModel.UserName,
                    PasswordHash = loginModel.Password
                };
                IdentityRole userRole = await GetUserRoleFromMoodle(loginModel.UserName);

                if (userRole != null)
                {
                    IdentityResult result = await _userManager.CreateAsync(user, loginModel.Password);
                    if (result.Succeeded)
                    {
                        await _userManager.AddToRoleAsync(user, userRole.Name);
                        return user;
                    }

                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("", error.Description);
                    }
                }

            }
            else
            {
                if (!ModelState.ContainsKey("connectionError"))
                {
                    ModelState.AddModelError("invalidLogin", "Неверный логин и/или пароль.");
                }
            }
            return null;
        }

        private async Task<string> GetAuthToken(LoginModel loginModel)
        {
            WebResponse authMoodleResponse = await SendMoodleRequestAsync(loginModel);
            string userToken = GetTokenFromResponse(authMoodleResponse);
            authMoodleResponse?.Dispose();
            return userToken;
        }

        private async Task<IdentityRole> GetUserRoleFromMoodle(string username)
        {
            HashSet<string> userRolesOnMoodle = GetRolesByUsername(username);

            if (userRolesOnMoodle == null)
                return null;

            IdentityRole userRole = await ChooseSuitableUserRole(userRolesOnMoodle);
            return userRole;
        }

        private HashSet<string> GetRolesByUsername(string username)
        {
            HashSet<string> userRolesOnMoodle = new HashSet<string>();

            //try
            //{
            //    using (var connection = MysqlConnectionProvider.CreateConnection())
            //    {
            //        connection.Open();
            //        string moodleQuery =
            //            @"select distinct role.archetype
            //            from mdl_user user join mdl_role_assignments ra
            //            on user.id=ra.userid
            //            join mdl_role role
            //            on role.id=ra.roleid
            //            where user.username=@username";
            //        MySqlCommand command = new MySqlCommand(moodleQuery, connection);
            //        command.Parameters.AddWithValue("username", username);

            //        var reader = command.ExecuteReader();

            //        while (reader.Read())
            //        {
            //            userRolesOnMoodle.Add(reader.GetString("archetype"));
            //        }
            //        return userRolesOnMoodle;
            //    }
            //}
            //catch(MySqlException ex)
            //{
            //    ModelState.AddModelError("connectionError", "Ошибка подключения к базе данных Moodle");
            //}
            //return null;

            try
            {
                using (var connection = PostgresConnectionProvider.CreateConnection())
                {
                    connection.Open();
                    string moodleQuery =
                        @"select distinct r.archetype
                        from mdl_user u join mdl_role_assignments ra
                        on u.id=ra.userid
                        join mdl_role r
                        on r.id=ra.roleid
                        where u.username=@username OR u.email=@username";
                    NpgsqlCommand command = new NpgsqlCommand(moodleQuery, connection);
                    command.Parameters.AddWithValue("username", username);

                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        userRolesOnMoodle.Add(reader.GetString(0));
                    }
                    return userRolesOnMoodle;
                }
            }
            catch (MySqlException ex)
            {
                ModelState.AddModelError("connectionError", "Ошибка подключения к базе данных Moodle");
            }
            return null;
        }

        private async Task<WebResponse> SendMoodleRequestAsync(LoginModel loginModel)
        {
            WebRequest authMoodleRequest =
                (HttpWebRequest)WebRequest
                    .Create(
                        $"http://{MoodleDomain}/login/token.php?username={loginModel.UserName}&password={loginModel.Password}&service={_webService}");
            authMoodleRequest.Method = "GET";
            authMoodleRequest.ContentType = "application/json";

            try
            {
                WebResponse authMoodleResponse = await authMoodleRequest.GetResponseAsync();
                return authMoodleResponse;
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("connectionError", "Ошибка с запросом на Moodle.");
            }
            return null;
        }

        private void CreateLocalUser(string nickname, string roleName)
        {
            using (IUnitOfWork unitOfWork = _unitOfWorkFactory.Create())
            {
                var user = _userService.GetUserByName(nickname);
                var role = _roleService.Get(roleName);

                if (user == null)
                    _userEntityService.Add(new Domain.Entities.User(nickname, role));

                unitOfWork.Commit();
            }
        }

        string GetTokenFromResponse(WebResponse response)
        {
            if (response != null)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                StringBuilder responseText = new StringBuilder();
                responseText.Append(reader.ReadToEnd());
                if (responseText.ToString().Contains("token"))
                {
                    JObject jdata = JObject.Parse(responseText.ToString());
                    return jdata["token"].Value<string>();
                }
            }
            return null;
        }

        private async Task<IdentityRole> ChooseSuitableUserRole(HashSet<string> roleList)
        {
            IdentityRole userRole = null;

            if (roleList.Count == 0 ||
                roleList.Contains("student"))
            {
                userRole = await _roleManager.FindByNameAsync("student");
            }
            if (roleList.Contains("teacher") ||
                roleList.Contains("editingteacher") ||
                roleList.Contains("coursecreator"))
            {
                userRole = await _roleManager.FindByNameAsync("teacher");
            }
            if (userRole == null)
            {
                ModelState.AddModelError("roleError","Для доступа к системе нужна роль студент, учитель или создатель курса");
            }
            return userRole;
        }

        private void BindNewUserToCourse(string username)
        {
            Domain.Entities.User user;
            IEnumerable<Domain.Entities.Course> courses;
            using (IUnitOfWork unitOfWork = _unitOfWorkFactory.Create())
            {
                user = _userService.GetUserByName(username);
                courses = _courseService.GetAllCourses();

                unitOfWork.Commit();
            }
            foreach (var course in courses)
            {
                using (IUnitOfWork unitOfWork = _unitOfWorkFactory.Create())
                {
                    if (_courseService.IsBindedUserToCourseOnMoodle(user, course))
                    {
                        _courseService.BindUserToCourse(user, course);
                    }
                    unitOfWork.Commit();
                }
            }
        }

        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Login");
        }
    }
}