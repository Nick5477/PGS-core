﻿namespace WebApp.Controllers.Submission.Profiles
{
    using AutoMapper;
    using ViewModels;

    public class SubmissionProfile : Profile
    {
        public SubmissionProfile()
        {
            CreateMap<Domain.Entities.Submission, SubmissionViewModel>();
        }
    }
}
