namespace WebApp.Controllers.Submission
{
    using Microsoft.AspNetCore.Mvc;
    using Domain.Services;
    using Domain.UnitOfWork;
    using System.Collections.Generic;
    using Domain.Entities;
    using AutoMapper;
    using Domain.Services.Rubric;
    using System.Linq;
    using ViewModels;
    using Domain.Services.CardinalGrade;
    using Domain.Services.Submission;

    public class SubmissionController : Controller
    {
        private readonly IEntityService<Submission> _submissionEntityService;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IMapper _mapper;
        private readonly IRubricService _rubricService;
        private readonly ICardinalGradeService _cardinalGradeService;
        private readonly IEntityService<User> _userEntityService;
        private readonly IEntityService<CardinalGrade> _cardinalGradeEntityService;
        private readonly IEntityService<Task> _taskEntityService;

        private readonly ISubmissionService _submissionService;

        public SubmissionController(
            IEntityService<Submission> submissionEntityService,
            IUnitOfWorkFactory unitOfWorkFactory,
            IMapper mapper,
            IRubricService rubricService,
            ICardinalGradeService cardinalGradeService,
            IEntityService<User> userEntityService,
            IEntityService<CardinalGrade> cardinalGradeEntityService,
            IEntityService<Task> taskEntityService,
            ISubmissionService submissionService)
        {
            _submissionEntityService = submissionEntityService;
            _unitOfWorkFactory = unitOfWorkFactory;
            _mapper = mapper;
            _rubricService = rubricService;
            _cardinalGradeService = cardinalGradeService;
            _userEntityService = userEntityService;
            _cardinalGradeEntityService = cardinalGradeEntityService;
            _taskEntityService = taskEntityService;
            _submissionService = submissionService;
        }

        [HttpGet]
        public IActionResult Get(string id, int number)
        {
            Submission submission;
            List<Rubric> rubrics;
            CardinalGrade cardinalGrade;
            string[] answers;
            using (IUnitOfWork unitOfWork = _unitOfWorkFactory.Create())
            {
                var user = _userEntityService.Get(HttpContext.User.Identity.Name);
                submission = _submissionEntityService.Get(id);
                rubrics = _rubricService.GetRubricByTask(submission.Task.Id).ToList();
                cardinalGrade = _cardinalGradeService.GetCardinalGradeBySubmissionAndUser(submission, user);
                answers = new string[rubrics.Count];

                int i = 0;
                if (cardinalGrade != null)
                {
                    foreach (var answer in cardinalGrade.Answers)
                    {
                        answers[i] = $"{answer.Key.Id}_{answer.Value.Id}";
                        i++;
                    }
                }
                
                unitOfWork.Commit();
            }

            SubmissionViewModel model = _mapper.Map<SubmissionViewModel>(submission);
            model.Rubrics = rubrics;
            model.WorkNumber = number;
            model.CardinalGrade = cardinalGrade;
            model.Answers = answers;

            return View(model);
        }

        [HttpPost]
        public IActionResult Get(SubmissionViewModel viewModel)
        {
            Submission submission;

            using (IUnitOfWork unitOfWork = _unitOfWorkFactory.Create())
            {
                var user = _userEntityService.Get(HttpContext.User.Identity.Name);
                submission = _submissionEntityService.Get(viewModel.Id);
                var rubrics = _rubricService.GetRubricByTask(submission.Task.Id);
                var cardinalGrade = _cardinalGradeService.GetCardinalGradeBySubmissionAndUser(submission, user);

                if (cardinalGrade == null)
                {
                    cardinalGrade = new CardinalGrade(
                        user,
                        submission);
                }
                else
                {
                    _cardinalGradeEntityService.Delete(cardinalGrade.Id);
                }
                
                int points = 0;
                Dictionary<Rubric, RubricLevel> answers = new Dictionary<Rubric, RubricLevel>();
                foreach (var answer in viewModel.Answers)
                {
                    string[] str_answer = answer.Split('_');
                    var rubric = rubrics.FirstOrDefault(r => r.Id == str_answer[0]);
                    var rubricLevel = rubric.Levels.FirstOrDefault(rl => rl.Id == str_answer[1]);
                    points += rubricLevel.Points;
                    answers.Add(rubric, rubricLevel);
                }

                cardinalGrade.Grade = points;
                cardinalGrade.Answers = answers;
                cardinalGrade.InitializeId();
                _cardinalGradeEntityService.Add(cardinalGrade);

                unitOfWork.Commit();
            }

            return RedirectToRoute(new
            {
                controller = "Task",
                action = "Get",
                id = submission.Task.Id
            });
        }

        [HttpGet]
        public bool CalculateCardinalGrades(string id)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var task = _taskEntityService.Get(id);
                _cardinalGradeService.CalculateGrades(task, 0.8, 0.1);

                unitOfWork.Commit();
            }

            return true;
        }

        [HttpGet]
        public bool LoadGradesToMoodle(string id)
        {
            bool result = true;
            Task task = null;
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                task = _taskEntityService.Get(id);
                

                unitOfWork.Commit();
            }

            result = _submissionService.LoadGradesToMoodle(task);

            return result;
        }
    }
}