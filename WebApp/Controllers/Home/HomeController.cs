﻿using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers.Home
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return RedirectToAction("List", "Course");
        }

        public IActionResult About()
        {
            return View();
        }

        public IActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public IActionResult AuthError()
        {
            return View();
        }
    }
}