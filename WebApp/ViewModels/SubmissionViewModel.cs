﻿namespace WebApp.ViewModels
{
    using Domain.Entities;
    using System.Collections.Generic;

    public class SubmissionViewModel
    {
        public string Id { get; set; }

        public Task Task { get; set; }

        public int Priority { get; set; }

        public string TextAnswer { get; set; }

        public int WorkNumber { get; set; }

        public File FileAnswer { get; set; }

        public List<Rubric> Rubrics { get; set; }

        public CardinalGrade CardinalGrade { get; set; }

        public string[] Answers { get; set; }
    }
}
