﻿namespace WebApp.ViewModels
{
    using System.Collections.Generic;
    using Domain.Entities;

    public class GradesTaskViewModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string CourseName { get; set; }

        public List<Submission> Submissions { get; set; }

        public long MaxGrade { get; set; }
    }
}
