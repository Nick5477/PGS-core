﻿namespace WebApp.ViewModels
{
    using System.Collections.Generic;

    using Domain.Entities;

    public class RubricViewModel
    {
        public string Id { get; private set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public Task Task { get; set; }

        public List<RubricLevel> Levels { get; set; }

        public RubricViewModel()
        {
            Levels = new List<RubricLevel>();
        }
    }
}
