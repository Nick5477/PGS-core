﻿namespace WebApp.ViewModels
{
    public class CourseViewModel
    {
        public string Id { get; set; }
        public string FullName { get; set; }
    }
}
