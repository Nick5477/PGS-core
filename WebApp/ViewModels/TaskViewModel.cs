﻿namespace WebApp.ViewModels
{
    using System;

    public class TaskViewModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime AllowSubmissionFromDate { get; set; }

        public DateTime DueDate { get; set; }

        public DateTime GradingDate { get; set; }

        public int MaxWorks { get; set; }

        public string CourseName { get; set; }
    }
}
