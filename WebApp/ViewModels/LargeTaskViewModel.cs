﻿namespace WebApp.ViewModels
{
    using System;
    using System.Collections.Generic;
    using Domain.Entities;

    public class LargeTaskViewModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime AllowSubmissionFromDate { get; set; }

        public DateTime DueDate { get; set; }

        public DateTime GradingDate { get; set; }

        public List<Submission> Submissions { get; set; }

        public bool IsAttachedSubmission { get; set; }

        public string CourseId { get; set; }

        public bool IsGradedSubmissions { get; set; }

        public PeerGradingEnum PeerGrading { get; set; }

        public List<CardinalGrade> CardinalGrades { get; set; }

        public List<Rubric> Rubric { get; set; }
    }
}
