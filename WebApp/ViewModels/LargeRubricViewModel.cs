﻿namespace WebApp.ViewModels
{
    using System.Collections.Generic;

    public class LargeRubricViewModel
    {
        public List<RubricViewModel> Rubrics { get; set; }

        public string TaskId { get; set; }

        public string TaskGradingDescription { get; set; }

        public LargeRubricViewModel()
        {
            Rubrics = new List<RubricViewModel>();
        }
    }
}
