﻿namespace WebApp.ViewModels
{
    using System.Collections.Generic;
    using Domain.Entities;

    public class OrdinalGradeTaskViewModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string CourseName { get; set; }

        public Dictionary<DistributionUnit, File> DistributionFile { get; set; }

        public List<Submission> Submissions { get; set; }

        public int MaxWorks { get; set; }
    }
}
