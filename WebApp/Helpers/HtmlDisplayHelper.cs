﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApp.Helpers
{
    public static class HtmlDisplayHelper
    {
        public static HtmlString DisplayHtmlString(this IHtmlHelper html, string item)
        {
            return new HtmlString(item);
        }
    }
}
