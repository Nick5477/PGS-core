﻿namespace WebApp.Modules
{
    using Autofac;
    using System.Reflection;
    using Domain.Commands;
    using Infrastructure.Db.Entities.Course.Commands;
    using global::Autofac;
    using Module = global::Autofac.Module;

    public class CommandsModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(AddCourseCommand).GetTypeInfo().Assembly).AsClosedTypesOf(typeof(ICommand<>));
            builder.RegisterType<CommandBuilder>().As<ICommandBuilder>();
            builder.RegisterTypedFactory<ICommandFactory>();
        }
    }
}