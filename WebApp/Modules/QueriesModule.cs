﻿namespace WebApp.Modules
{
    using System.Reflection;
    using Autofac;
    using Domain.Queries;
    using global::Autofac;
    using Module = global::Autofac.Module;
    using Infrastructure.Db.Entities.Course.Queries;

    public class QueriesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(SyncCoursesQuery).GetTypeInfo().Assembly).AsClosedTypesOf(typeof(IQuery<,>));
            builder.RegisterGeneric(typeof(QueryFor<>)).As(typeof(IQueryFor<>));
            builder.RegisterTypedFactory<IQueryBuilder>();
            builder.RegisterTypedFactory<IQueryFactory>();
        }
    }
}