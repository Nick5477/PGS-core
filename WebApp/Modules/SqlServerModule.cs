﻿namespace WebApp.Modules
{
    using Domain.Repositories;

    using global::Autofac;

    using Infrastructure.Db.Commands;
    using Infrastructure.Db.Connections;
    using Infrastructure.Db.Repositories;
    using Infrastructure.Db.Sqlite;

    using Microsoft.Extensions.Configuration;
    using System.Reflection;

    using Infrastructure.Db.SqlServer;

    using Module = global::Autofac.Module;

    public class SqlServerModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //builder.RegisterAssemblyTypes(typeof(UserRepository).GetTypeInfo().Assembly)
            //    .AsClosedTypesOf(typeof(IRepository<>));
            //builder.RegisterType<MSSQLCommandFactory>()
            //    .As<IDbCommandFactory>();
            //builder.Register(c =>
            //        new MSSQLConnectionFactory(
            //            c.Resolve<IConfigurationRoot>().GetConnectionString("SQLServerConnectionString")))
            //    .As<IDbConnectionFactory>().SingleInstance();
        }
    }
}
