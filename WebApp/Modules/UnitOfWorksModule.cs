﻿namespace WebApp.Modules
{
    using Domain.UnitOfWork;
    using Infrastructure.Db.UnitOfWork;
    using global::Autofac;
    using Module = global::Autofac.Module;

    public class UnitOfWorksModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<DbUnitOfWorkFactory>().As<IUnitOfWorkFactory>().InstancePerLifetimeScope(); //per request
            builder.RegisterType<DbUnitOfWork>().As<IUnitOfWork>();
        }
    }
}