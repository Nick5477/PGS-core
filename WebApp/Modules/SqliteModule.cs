﻿namespace WebApp.Modules
{
    using System.IO;
    using System.Reflection;
    using Domain.Repositories;
    using Infrastructure.Db.Commands;
    using Infrastructure.Db.Connections;
    using Infrastructure.Db.Repositories;
    using Infrastructure.Db.Sqlite;
    using Microsoft.Extensions.Configuration;
    using global::Autofac;
    using Module = global::Autofac.Module;

    public class SqliteModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(UserRepository).GetTypeInfo().Assembly)
                .AsClosedTypesOf(typeof(IRepository<>));
            builder.RegisterType<SqliteCommandFactory>()
                .As<IDbCommandFactory>();
            builder.Register(c =>
                    new SqliteConnectionFactory(
                    c.Resolve<IConfigurationRoot>().GetConnectionString("TestConnectionString").Replace("{App_dir}", Directory.GetCurrentDirectory())))
                .As<IDbConnectionFactory>().SingleInstance();
        }
    }
}