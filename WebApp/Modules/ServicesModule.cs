﻿namespace WebApp.Modules
{
    using Domain.Services;
    using Domain.Services.User;
    using global::Autofac;
    using Module = global::Autofac.Module;
    using System.Reflection;
    using Domain.Services.DistributionUnit;

    public class ServicesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(UserService).GetTypeInfo().Assembly)
                .AsClosedTypesOf(typeof(IEntityService<>))
                .AsImplementedInterfaces();
            builder.RegisterType<DistributionService>().As<IDistributionService>();
        }
    }
}