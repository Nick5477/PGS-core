﻿namespace WebApp.Modules
{
    using global::Autofac;
    using Module = global::Autofac.Module;
    using Infrastructure.Db.Transactions;

    public class TransactionsModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<DbTransactionProvider>()
                .As<IDbTransactionProvider>()
                .InstancePerLifetimeScope();
        }
    }
}